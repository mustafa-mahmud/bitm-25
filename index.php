  <?php
$a=10;
$b=$a; // 10 integer;
$c="$b";// 10 string;
$d='10';// 10 string;
if($c===$d){
    echo "Good";
}
else{
    echo "Bad";
}
echo "<br/>";
?>

<?php
    $a="bangladesh";
    $b="$a";
    $c='$b';
//        echo $c;
    echo " ' ". $b." ' ";
    ?>
<?php
$a="";
echo (empty($a))?"Opps not avaible data":"there is data";
?>
<?php
echo "<br/>"."gettype()";

$data=array(NULL,TRUE,10,"text",2.5);
foreach ($data as $list){
    echo "<pre/>";
    echo gettype($list);
}
?>

<?php
echo "<br/>"."is_array()";

$a=array("a","b","c");
if(is_array($a)){
    echo "<br/>"."This is an array ". $a[1];
}
else{
    echo "this is not an array";
}

echo "<br/>";
print_r($a);
$b=array("first"=>"Rony","last_name"=>"Bony");
echo "<pre/>";
print_r($b);
echo $b["first"];
echo "<pre/>";
$c=array(array(array(array(array(array())))),array());
print_r($c);
$d=array("bd","next"=>array("dhaka","ctg"));
print_r($d);
//echo $next[1];
        
?>

<?php
echo "<br/>"."is_bool()";
$a=false;
$b=10;
if(is_bool($a)){
    echo "This is Bool";
}
else{
    echo "No Bool";
}

?>

<?php
$var="some";
var_dump(boolval($var));
echo boolval($var);
?>

<?php
echo "<br/>"."isset()"."<br/>";
$a="";
if(isset($a) && !empty($a)){
    echo $a;
}
 else {
    echo "Not Value";
}
?>

<?php
echo "<br/>"."serialize"."<br/>";
        
$arr=array("1020","dhaka","bd");
print_r($arr);
$serializedata=  serialize($arr);

echo $serializedata;

$unserialize=$serializedata;
print_r(unserialize($unserialize));
?>

<?php
echo "<br/>"."unset()"."<br/>";
$a=10;
echo $a;
unset($a);
$a=20;
echo $a;
?>

<?php
echo "<br/>"."var_dump()"."<br/>";

$a=array("a","b","c");
print_r($a);
echo "<br/>";
var_dump($a);
?>