<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Circle Hover Effects with CSS Transitions" />
<meta name="keywords" content="circle, border-radius, hover, css3, transition" />
<meta name="author" content="Mohin Jaki" />
<link rel="icon" href="">
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="asset/front_end/css/font-awesome.min.css" /><!-- FONT AWESOME (UI) -->
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /><!-- BOOTSTRAP (CDN) -->
<link rel="stylesheet" type="text/css" href="asset/front_end/css/bootstrap.min.css" /><!-- BOOTSTRAP (UI) -->
<link rel="stylesheet" type="text/css" href="asset/front_end/css/normalize.css" /><!-- NORMALIZE -->
<link rel="stylesheet" type="text/css" href="asset/front_end/css/lightbox.min.css" /><!-- NORMALIZE -->
<link rel="stylesheet" type="text/css" href="asset/front_end/css/stl.css" /><!-- CUSTOM -->

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700' rel='stylesheet' type='text/css' />
<script type="text/javascript" src="asset/front_end/js/modernizr.custom.79639.js"></script> 
<!--[if lte IE 8]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->

<style>
    .btn-req-friend{
        font-size: 2rem;
        font-weight: bold;
        border-left: 2px solid #111;
        box-sizing: border-box;
        transition: 0.5s;
    }
    .btn-req-friend{
        
    }
</style>