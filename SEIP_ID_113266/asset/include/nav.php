<nav class="navbar navbar-inverse">
            <div class="col-md-7">
                <form class="navbar-form" action="" method="post">
                    <div class="form-group col-md-12">
                        <div class="input-group col-md-12">
                            <input type="text" class="form-control">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
                        </div>
                    </div>
                </form>
            </div><!-- end of search box -->

            <div class="container-fluid col-md-5">
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav pull-right">
                        <li class="active"><a href="home.php">Home</a></li>
                        <li><a href="profile.php">Profile</a></li>
                        <li><a href="LittleShop/LittleShop.php">Little Shop</a></li>
                        <li><a href="shopping.php">Shopping <i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="advice.php">Help</a></li>
                                <li><a href="advice.php">Advice</a></li>
                                <li><a href="setting.php">Setting</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="?status=user_logout">Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- end of navigation bar -->
        </nav>