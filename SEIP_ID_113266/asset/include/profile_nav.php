<div class="pro-menu">
                <nav class="navbar navbar-inverse">
                    <ul class="nav navbar-nav pro-nav">
                        <li style="margin: 6px 0 0 -80px; color: #fff; font-size: 3rem; font-weight: bold; text-shadow: 2px 2px 2px #666;"><?php if(loged_in()){ echo $_SESSION['first_name'] .' '; echo $_SESSION['last_name']; } ?></li>
                        <li style="margin-top: 10px; color: #222;">~~~~~</li>
                        <li><a href="profile.php" class="btn btn-primary pro-btn">ZONE</a></li>
                        <li><a href="about.php" class="btn btn-primary pro-btn">ABOUT</a></li>
                        <li><a href="my_friends.php" class="btn btn-primary pro-btn">FRIENDS</a></li>
                        <li><a href="my_photos.php" class="btn btn-primary pro-btn" style="border-right: 1px solid #fff;">PHOTO</a></li>
                        <li style="margin-top: 10px; color: #222;">~~~~~~~~</li>
                        <li><a href="request_friends.php" class="btn" style="background-color: #000; font-size: 2rem; font-weight: bold; box-shadow: 4px 4px 4px #111; text-shadow: 0.5px 0.5px 1px #fff; color: #fff; border-right: 1px solid #fff;"><i class="fa fa-user-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="btn" style="background-color: #000; font-size: 2rem; font-weight: bold; box-shadow: 4px 4px 4px #111; text-shadow: 0.5px 0.5px 1px #fff; color: #fff;"><i class="fa fa-commenting-o" aria-hidden="true"></i></a></li>
                    </ul>
                </nav>
            </div><!-- end of pro-menu -->