<?php
include_once ('raw/init.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <title>MSOUL | Recover Password</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Circle Hover Effects with CSS Transitions" />
        <meta name="keywords" content="circle, border-radius, hover, css3, transition" />
        <meta name="author" content="Codrops" />
        <link rel="icon" href="asset/front_end/images/icon.ico"> 
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /><!-- BOOTSTRAP (CDN) -->
        <link rel="stylesheet" type="text/css" href="asset/front_end/css/bootstrap.min.css" /><!-- BOOTSTRAP (UI) -->
        <link rel="stylesheet" type="text/css" href="asset/front_end/css/normalize.css" /><!-- NORMALIZE -->
        <link rel="stylesheet" type="text/css" href="asset/front_end/css/style.css" /><!-- CUSTOM -->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700' rel='stylesheet' type='text/css' />
        <script type="text/javascript" src="asset/front_end/js/modernizr.custom.79639.js"></script> 
        <!--[if lte IE 8]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
        <style>
            .forget_password{
                margin: 10rem 0 0 30rem;
            }
            h1{
                padding-left: 9rem;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="col-md-6">
                <div class="img"></div>
                <div>
                    <a href="index.php" title="msoul" class="">ManSoul</a>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 forget_password">
                    <h1>Submit Your Email</h1>
                    <form id="login-form" action="" method="post" role="form" style="display: block;">
                        <div class="form-group col-md-10">
                            <input type="email" name="email" placeholder="YOUR EMAIL" class="form-control" required="required" regexp="JSVAL_RX_EMAIL" err="Email Address">
                            <input type="hidden" name="token" tabindex="token" class="form-control" value="<?php echo token_generator(); ?>">
                        </div>
                        <div class="form-group col-md-10">
                            <input type="submit" name="R_btn" value="SUBMIT" class="form-control btn btn-default btn-block">
                        </div>
                    </form>
                    <div class="col-md-12">
                        <textarea style="color:#fff; font-size: 1.5rem;" class="col-md-10 text-justify" readonly><?php recovered_password(); ?></textarea>
                    </div>
                </div>
            </div>
        </div>

        <!-- JAVASCRIPT -->

        <!-- JS jQUERY 2 -->
        <script src="asset/front_end/js/jquery.min.js"></script>

        <!-- JS jQUERY 1.9.0-->
        <!--<script src="asset/front_end/js/jquery-1.9.0.min.js"></script>-->

        <!-- Bootstrap -->
        <script src="asset/front_end/js/bootstrap.js"></script>

        <!-- Jsval (VALIDATION) -->
        <script src="asset/front_end/js/jsval.js"></script>

        <!-- Ajax (EMAIL CHECKING) -->
        <script src="asset/front_end/js/ajax.js"></script>

        <!-- CUSTOM JS -->
        <script type="asset/front_end/text/javascript" src="js/script.js"></script>

        <script type="asset/front_end/text/javascript" src="js/script.js"></script>
    </body>
</html>
