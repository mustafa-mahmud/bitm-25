<?php
    require_once ('raw/init.php');

    $user_id = $_SESSION['user_id'];
    if($user_id == NULL)
    {
        redirect("index.php");
    }

    if(isset($_GET['status']))
    {
        user_logout();
    }
    
    require_once ('classes/cart.php');
    $obj_cart = new Cart();
    
    if(isset($_POST['update_quantity']))
    {
    $obj_cart->update_product_sales_quantity($_POST);
    }
    
    if(isset($_GET['status']))
    {
        if($_GET['status'] == 'remove')
        {
            $cart_id = $_GET['cart_id'];
            $obj_cart->remove_cart_product($cart_id);
        }
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Little Shop | Msoul</title>
        <?php include_once ('asset/include/header.php'); ?>
    </head>
    <body class="LittleShop">
        <!-- start of nav -->
            <?php include_once ('asset/include/nav.php'); ?>
        <!-- end of nav -->
        <div class="container">
            <?php
                if(isset($pages))
                {
                    if($pages == 'ordering_address')
                    {
                        include_once ('pages/ordering_address_content.php');
                    }
                    elseif($pages == 'payment')
                    {
                        include_once ('pages/payment_content.php');
                    }
                }
                else
                {
                    include_once ('shopping_content.php');
                }
            ?>
            <!--------------- FOOTER ---------------->
            <?php include_once ('asset/include/footer.php'); ?>
        </div>
    </body>
</html>