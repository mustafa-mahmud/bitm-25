<?php
session_start();
ob_start();
require_once ('../classes/application.php');
$obj_appication = new Application();

$query_result = $obj_appication->select_published_category();

if (isset($_GET['option'])) {
    $category_id = $_GET['category_id'];
    if ($_GET['option'] == 'category') {
        $product_result = $obj_appication->select_all_published_category_by_id($category_id);
    }
} else {
    $product_result = $obj_appication->select_all_published_product();
}

if (isset($_GET['product'])) {
    $product_id = $_GET['product_id'];
    if ($_GET['product'] == 'product') {
        $product_result = $obj_appication->select_product_by_product_id($product_id);
    }
    $result = mysqli_fetch_assoc($product_result);
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Little Shop | Msoul</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Circle Hover Effects with CSS Transitions" />
        <meta name="keywords" content="circle, border-radius, hover, css3, transition" />
        <meta name="author" content="Codrops" />
        <link rel="icon" href="">
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../asset/front_end/css/font-awesome.min.css" /><!-- FONT AWESOME (UI) -->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /><!-- BOOTSTRAP (CDN) -->
        <link rel="stylesheet" type="text/css" href="../asset/front_end/css/bootstrap.min.css" /><!-- BOOTSTRAP (UI) -->
        <link rel="stylesheet" type="text/css" href="../asset/front_end/css/normalize.css" /><!-- NORMALIZE -->
        <link rel="stylesheet" type="text/css" href="../asset/front_end/css/simple-sidebar.css" /><!-- SIMPLE SLIDER -->
        <link rel="stylesheet" type="text/css" href="../asset/front_end/css/lightbox.min.css" /><!-- LIGHT BOX -->
        <link rel="stylesheet" type="text/css" href="../asset/front_end/css/stl.css" /><!-- CUSTOM -->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700' rel='stylesheet' type='text/css' />
        <script type="text/javascript" src="../asset/front_end/js/modernizr.custom.79639.js"></script> 
        <!--[if lte IE 8]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
    </head>
    <body class="LittleShop">
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand"><a href="LittleShop.php">ManSoul Little Shop</a></li>
                    <li><a href="index.php">ManSoul</a></li>
                    <li><a href="cart.php">Your Cart</a></li>
                    <br>
                <?php while ($row = mysqli_fetch_assoc($query_result)) { ?>
                        <li><a href="LittleShop.php?option=category&category_id=<?php echo $row['category_id']; ?>"><?php echo $row['category_name']; ?></a></li>
                <?php } ?>
                </ul>
            </div><!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">

            <?php
            if (isset($pages))
                {
                if ($pages == 'product_view')
                {
                    include ('pages/product_details_content.php');
                }
                elseif ($pages == 'cart')
                {
                    include ('pages/cart_content.php');
                }
                elseif( $pages == 'ShipPage')
                {
                    include_once ('pages/shiping_page_content.php');
                }
            }
            else
            {
                include ('page_content.php');
            }
            ?>

                </div><!-- end of container-fluid -->
            </div><!-- /#page-content-wrapper -->

        </div><!-- /#wrapper -->
        <footer id="footer">
            <p class="pull-right">Copyright &COPY; 2016 by <a href="#" title="http://www.jqurity.com">Mohin Jaki</a> &squf; All rights reserved</p>
        </footer>
        <!-- JAVASCRIPT -->
        <!-- JS jQUERY -->
        <script src="../asset/front_end/js/jquery.min.js"></script>
        <!-- BOOTSTRAP -->
        <script src="../asset/front_end/js/bootstrap.min.js"></script>
        <!-- LIGHT BOX -->
        <script src="../asset/front_end/js/lightbox-plus-jquery.min.js"></script>
        <!-- CUSTOM JS -->
        <script type="text/javascript" src="../asset/front_end/js/script.js"></script>

        <script>
            $(document).ready(function () {

                var docHeight = $(window).height();
                var footerHeight = $('#footer').height();
                var footerTop = $('#footer').position().top + footerHeight;

                if (footerTop < docHeight) {
                    $('#footer').css('margin-top', 10 + (docHeight - footerTop) + 'px');
                }
            });
        </script>
    </body>
</html>
