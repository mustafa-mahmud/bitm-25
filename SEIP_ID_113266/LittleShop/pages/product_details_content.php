<?php
require_once ('../classes/cart.php');
$obj_cart = new Cart();

if(isset($_POST['add_to_cart']))
{
    $obj_cart->add_to_cart($_POST);
}
?>

<div class="featured-product">
    <div class="row">
        <form action="" method="post">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <a href="../mAdmin403/<?php echo $result['product_image']; ?>" data-lightbox="example-1"><img class="img-thumbnail" src="../mAdmin403/<?php echo $result['product_image']; ?>" alt="image-1" style="width: 480px; height: 380px;" /></a>
                        <div class="clearfix"></div>
                        <br>
                        <a href="../mAdmin403/<?php echo $result['product_image']; ?>" data-lightbox="example-2" data-title="<?php echo $result['product_name'];?>"><img class="img-thumbnail" src="../mAdmin403/<?php echo $result['product_image']; ?>" style="width: 233px; height: 150px;" alt="image-1"/></a>
                        <a href="../mAdmin403/<?php echo $result['product_image']; ?>" data-lightbox="example-2" data-title="<?php echo $result['product_name'];?>"><img class="img-thumbnail" src="../mAdmin403/<?php echo $result['product_image']; ?>" style="width: 233px; height: 150px;" alt="image-1"/></a>
                    </div>
                </div><!-- end of col-md-6 -->
                <div class="col-md-6">
                    <div class="page-header">
                        <h1><?php echo $result['product_name'];?></h1>
                        <h2>BDT <?php echo $result['product_price']; ?></h2>
                        <h2><input type="text" name="sales_quantity" value="1" style="color: #000; width: 13.5rem" /> <button type="submit" name="add_to_cart" class="btn btn-lg btn-danger">ADD TO CART</button> <button type="submit" class="btn btn-lg btn-success">ORDER</button></h2>
                    </div>
                    <h4>PRODUCT DETAILS</h4>
                    <p><?php echo $result['product_short_description']; ?></p>
                    <hr>
                    <h4>MORE INFORMATION</h4>
                    <p><?php echo $result['product_long_description']; ?></p>
                </div> <!-- end of col-md-6 -->
            </div><!--  end of col-md-12 -->
            <input type="hidden" name="product_id" value="<?php echo $result['product_id']; ?>">
        </form>

        <div class="clearfix"></div><br>
        
        <div class="col-md-4">
            <div class="thumbnail">
                <img class="img-responsive" src="http://placehold.it/600x300" alt="...">
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi vel ab ea sit natus veritatis at iste quia aliquid dignissimos placeat illo, eius quaerat magnam molestiae, repellendus consequatur amet rerum deserunt</p>

                    <p><a href="#" class="btn btn-primary" role="button">Add to Cart</a> <a href="show_details.html" class="btn btn-success" role="button">Quick View</a></p>
                </div><!-- /caption -->
            </div><!-- /thumbnail -->
        </div>
        
        <div class="col-md-4">
            <div class="thumbnail">
                <img class="img-responsive" src="http://placehold.it/600x300" alt="...">
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi vel ab ea sit natus veritatis at iste quia aliquid dignissimos placeat illo, eius quaerat magnam molestiae, repellendus consequatur amet rerum deserunt</p>

                    <p><a href="#" class="btn btn-primary" role="button">Add to Cart</a> <a href="show_details.html" class="btn btn-success" role="button">Quick View</a></p>
                </div><!-- /caption -->
            </div><!-- /thumbnail -->
        </div>
        
        <div class="col-md-4">
            <div class="thumbnail">
                <img class="img-responsive" src="http://placehold.it/600x300" alt="...">
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi vel ab ea sit natus veritatis at iste quia aliquid dignissimos placeat illo, eius quaerat magnam molestiae, repellendus consequatur amet rerum deserunt</p>

                    <p><a href="#" class="btn btn-primary" role="button">Add to Cart</a> <a href="show_details.html" class="btn btn-success" role="button">Quick View</a></p>
                </div><!-- /caption -->
            </div><!-- /thumbnail -->
        </div>
    </div> <!-- row -->
</div>