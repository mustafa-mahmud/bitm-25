<div class="row">
    <div class="col-lg-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="images/apple-watch.jpg" alt="Apple Watch" class="l-image">
                    <div class="carousel-caption">
                        <h2 class="pull-right">Some Text Here About Apple Watch</h2>
                    </div>
                </div>
                <div class="item">
                    <img src="images/canon.jpg" alt="Canon" class="l-image">
                    <div class="carousel-caption">
                        <h2 class="pull-right">Some Text Here About Canon DSLR</h2>
                    </div>
                </div>
                <div class="item">
                    <img src="images/nikon.jpg" alt="Nikon" class="l-image">
                    <div class="carousel-caption">
                        <h2 class="pull-right">Some Text Here About Nikon DSLR</h2>
                    </div>
                </div>
            </div><!-- end of carousel-inner -->
        </div><!-- end of carousel_slider -->
        <div class="clearfix"></div>
        <div>

        </div>
    </div><!-- end of col-lg-12 -->
</div><!-- end of row -->