<?php
require_once ('../classes/cart.php');
$obj_cart = new Cart();

if(isset($_POST['add_to_cart']))
{
    $obj_cart->add_to_cart($_POST);
}

?>

<div class="row">
    <div class="col-lg-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="images/apple-watch.jpg" alt="Apple Watch" class="l-image">
                    <div class="carousel-caption">
                        <h2 class="pull-right">Some Text Here About Apple Watch</h2>
                    </div>
                </div>
                <div class="item">
                    <img src="images/canon.jpg" alt="Canon" class="l-image">
                    <div class="carousel-caption">
                        <h2 class="pull-right">Some Text Here About Canon DSLR</h2>
                    </div>
                </div>
                <div class="item">
                    <img src="images/nikon.jpg" alt="Nikon" class="l-image">
                    <div class="carousel-caption">
                        <h2 class="pull-right">Some Text Here About Nikon DSLR</h2>
                    </div>
                </div>
            </div><!-- end of carousel-inner -->
        </div><!-- end of carousel_slider -->
        <div class="clearfix"></div>
        <div>

        </div>
    </div><!-- end of col-lg-12 -->
</div><!-- end of row -->

<div class="featured-product">
    <div class="row">
        <?php while ($row = mysqli_fetch_assoc($product_result)) {?>
        <div class="col-md-4">
            <div class="thumbnail">
                <img class="img-responsive img-thumbnail" src="../mAdmin403/<?php echo $row['product_image']; ?>" alt="product image" style="width: 300px; height: 200px;">
                <form action="" method="post">
                    <div class="caption">
                        <h3><?php echo $row['product_name']; ?></h3>
                        <p><?php echo $row['product_short_description'];?></p>

                        <p><button type="submit" name="add_to_cart" class="btn btn-primary">Add to Cart</button> <a href="product_details.php?product=product&product_id=<?php echo $row['product_id']; ?>" class="btn btn-success" role="button">Quick View</a></p>
                    </div><!-- /caption -->
                    <input type="hidden" name="sales_quantity" value="1">
                    <input type="hidden" name="product_id" value="<?php echo $row['product_id']; ?>">
                </form>
            </div><!-- /thumbnail -->
        </div>
        <?php }?>
    </div><!-- row -->
</div>