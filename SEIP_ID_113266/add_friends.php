<?php
include_once ('./raw/init.php');
$query_result = select_all_users_info();

$user_id = $_SESSION['user_id'];

$request_list_pending=[];
$result_pending = select_from_pending_list($user_id);
$result_friends = select_from_friend_list($user_id);

if(isset($_GET['member']))
{
    if(isset($_GET['user_id']))
    {
        $user_id = $_GET['user_id'];
        if($_GET['member'] == 'user')
        {
            select_all_user_by_user_id($user_id);
        }
    }
    
    if(isset($_GET['r_id']) && isset($_GET['s_id']))
    {
        $id = $_GET['r_id'];
        if(!isset($request_list[$id])){
            insert_into_pending_list($_GET['r_id'],$_GET['s_id']);
        } 
       
    }
}


if($user_id == NULL)
{
    rdirect("index.php");
}

if(isset($_GET['status']))
{
    user_logout();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Add Friends</title>
        <?php include_once ('./asset/include/header.php'); ?>
    </head>
    <body>
        <!-- start of nav -->
        <?php include_once ('asset/include/nav.php'); ?>
        <!-- end of nav -->
        <section class="home-container">
            <div class="row">
                <div class="col-md-9">
                    <div class="container">
                        <?php while ($row = mysqli_fetch_assoc($query_result)) { 
                            if($row['user_id'] != $user_id){
                            
                            ?>
                            
                            
                            <div class="col-md-8">
                                <div class="col-md-6" align="center">
                                    <img src="<?php echo $row['users_avator']; ?>" alt="<?php echo $row['username']; ?>" class="img-thumbnail" style="width: 200px; height: 200px;">
                                    <div class="text-center"><b><a href="user_id.php?member=user&user_id=<?php echo $row['user_id']; ?>" style="text-decoration: none;"><?php echo $row['first_name'] . ' ' .$row['last_name']; ?></a></b></div>
                                </div>
                                <div class="col-md-6">
                                    <p><a href="?member=user&r_id=<?php echo $row['user_id']?>&s_id=<?php echo $_SESSION['user_id']; ?>" class="btn btn-primary btn-block"><i class="fa fa-user"></i>
                                        <?php
                                            $relation = 0;
                                            foreach ($result_pending  as $request)
                                            {
                                                if($request['reciever_id'] == $row['user_id'])
                                                {
                                                    $relation = 1;
                                                }
                                            }
                                            
                                            foreach ($result_friends as $request)
                                            {
                                                if($request['first_friend_id'] == $row['user_id'] || 
                                                        $request['second_friend_id'] == $row['user_id'])
                                                {
                                                    $relation = 2;
                                                }
                                            }
                                            
                                            if($relation == 0)
                                            {
                                                echo 'Add Friend';
                                            }
                                            elseif($relation == 1)
                                            {
                                                echo 'Request Send';
                                            }
                                            elseif($relation == 2)
                                            {
                                                echo 'Friend';
                                            }
                                        ?>
                                        </a></p>
                                    <p><a href="#" class="btn btn-primary btn-block"><i class="fa fa-envelope"></i> Send Message</a></p>
                                    <p><a href="user_id.php?member=user&user_id=<?php echo $row['user_id']; ?>" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> View Profile</a></p>
                                </div>
                            </div><!-- end of user one -->
                            
                            
                        <?php }} ?>
                    </div><!-- end of container -->
                </div><!-- end of col-md-9 -->
                <div class="col-md-3">
                    <div class="list-group">
                        <p class="list-group-item">MAN SOUL</p>
                        <a href="home.php" class="list-group-item">News Grounds</a>
                    </div><!-- end of side menu -->
                </div><!-- end of col-md-3 -->
            </div><!-- end of row -->

            <form class="modal fade" id="updateCreditCard">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            <h4 class="modal-title"><span class="fa fa-users"></span> Create New Room</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="roomName">Room Name</label>
                                <input type="text" placeholder="room name" class="form-control" id="roomName">
                            </div>
                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" name="privacy">Public Room</label>
                                <label class="radio-inline"><input type="radio" name="privacy">Closed Room</label>
                                <label class="radio-inline"><input type="radio" name="privacy">Protected Room</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <a href="room.html" class="btn btn-primary">Create Room</a>
                        </div>
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </form>
        </section><!-- end of section -->
        <?php include_once ('asset/include/footer.php'); ?>
    </body>
</html>
