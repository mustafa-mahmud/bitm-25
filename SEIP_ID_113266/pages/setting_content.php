<?php
include_once ('raw/init.php');
$user_id = $_SESSION['user_id'];

if(isset($_POST['btn_password_change']))
{
    password_reset_user($user_id);
}

if(isset($_POST['btn_change_name']))
{
    user_name_change($_POST,$user_id);
}


?>

<?php include_once 'asset/include/cover.php'; ?>
<?php include_once 'asset/include/profile_nav.php'; ?>

<div class="container">
    <div class="page-header">
        <h2>SETTING <span class="text-success" style="font-size: 15px; font-weight: bold"><?php display_message(); ?></span></h2>
    </div>
    <form action="" method="post" onsubmit="validateStandard(this)">
        <div class="row">
            <div class="col-md-3 left-side-edu">
                <h3><b>Change Password</b></h3>
            </div><!-- end of left-side-edu -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="password" class="col-sm-3 form-control-label">Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" placeholder="Password" class="form-control" required="required">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="re-password" class="col-sm-3 form-control-label">Re-Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="confirm_password" equals="password" placeholder="Re-Password" err="Password and Repassword mustbe same" class="form-control" required="required">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <input type="submit" name="btn_password_change" value="Change Password" class="btn btn-primary btn-block">
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
    </form>
        <!-- end of education section -->
    <form action="" method="post">
        <div class="row">
            <div class="col-md-3 left-side-home">
                <h3><b>Change Your Name</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="first_name" class="col-sm-3 form-control-label">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="first_name" placeholder="First Name" class="form-control" required="required">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="last_name" class="col-sm-3 form-control-label">Last Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="last_name" placeholder="Last Name" class="form-control" required="required">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <input type="submit" name="btn_change_name" value="Change Name" class="btn btn-primary btn-block">
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
    </form>
        <!-- End of Zone You Leave Section -->
</div>