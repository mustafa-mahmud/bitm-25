<?php
require_once ('classes/shipping.php');
$obj_ship = new Shipping();

if(isset($_POST['btn']))
{
    $obj_ship->save_order_info($_POST);
}

?>

<div class="page-header">
    <h3>Payment Information <span class="pull-right"><a href="ordering_address.php" class="btn btn-danger"><i class="fa fa-backward"></i> BACK</a></span></h3>
</div>
<form action="" method="post">
    <div class="col-md-6" style="background-color: rgba(0,0,0,0.5); font-size: 2rem; color: #fff; padding: 15px;" />
        <h4 class="text-center">Please Select Your Payment Method</h4>
        <div class="form-group row" style="margin-left: 20px;">
            <label><input type="radio" name="payment" value="case_on_delivery"> Case On Delivery</label>
            <br>
            <label><input type="radio" name="payment" value="paypal"> Paypal</label>
        </div>
        <input type="submit" name="btn" value="Confirm Order" class="btn btn-success btn-block" />
    </div>
</form>