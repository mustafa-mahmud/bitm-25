<?php
require_once ('classes/shipping.php');

$obj_ship = new Shipping();

if(isset($_POST['btn']))
{
    $obj_ship->save_product_shipping_info($_POST);
}

?>

<div class="page-header">
    <h3>Privet Information for Shipping 
        <span class="pull-right"><a href="shopping.php" class="btn btn-danger"><i class="fa fa-backward" aria-hidden="true"></i> SELECTED PRODUCT</a> 
            <a href="payment.php" class="btn btn-success">PAYMENT <i class="fa fa-forward" aria-hidden="true"></i></a>
    </span></h3>
</div>
<form action="" method="post" style="font-size: 2rem;">
    <div class="col-md-9">
        <div class="form-group row">
            <label for="full_name" class="col-sm-3 form-control-label">Full Name</label>
            <div class="col-sm-9">
                <input name="full_name" type="text" class="form-control" id="full_name" required="required" />
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group row">
            <label for="email" class="col-sm-3 form-control-label">Email</label>
            <div class="col-sm-9">
                <input name="email_address" type="email" class="form-control" id="email" required="required" />
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group row">
            <label for="address" class="col-sm-3 form-control-label">Address</label>
            <div class="col-sm-9">
                <textarea name="address" class="form-control" id="address" required="required"></textarea>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group row">
            <label for="phone_number" class="col-sm-3 form-control-label">Phone Number</label>
            <div class="col-sm-9">
                <input name="phone_number" type="number" class="form-control" id="phone_number" required="required" />
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group row">
            <label for="city" class="col-sm-3 form-control-label">City</label>
            <div class="col-sm-9">
                <input name="city" type="text" class="form-control" id="city" required="required" />
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group row">
            <label for="country" class="col-sm-3 form-control-label">Country</label>
            <div class="col-sm-9">
                <select name="country" class="form-control" id="country" required="required" />
                <option>Choose Your Country</option>
                <option value="1">Bangladesh</option>
                <option value="2">Pakistan</option>
                <option value="3">India</option>
                <option value="4">Sri Lanka</option>
                <option value="5">Nepal</option>
                <option value="6">Vutan</option>
                <option value="7">Maldip</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group row">
            <label for="zip_code" class="col-sm-3 form-control-label">Zip Code</label>
            <div class="col-sm-9">
                <input name="zip_code" type="text" class="form-control" id="zip_code" required="required">
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group row">
            <label for="submit" class="col-sm-3 form-control-label"></label>
            <div class="col-sm-9">
                <input name="btn" type="submit" style="font-weight: bold;" class="form-control btn btn-default btn-block" id="submit">
            </div>
        </div>
    </div>
</form>