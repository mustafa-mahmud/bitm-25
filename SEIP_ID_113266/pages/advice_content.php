<?php
include_once ('raw/init.php');
$user_id = $_SESSION['user_id'];

if(isset($_POST['btn_help']))
{
    user_help_message($_POST,$user_id);
}

if(isset($_POST['btn_advice']))
{
    user_avvice_message($_POST,$user_id);
}

?>


<?php include_once 'asset/include/cover.php'; ?>
<?php include_once 'asset/include/profile_nav.php'; ?>

<div class="container">
    <div class="page-header">
        <h2>HELP AND ADVICE <span class="text-success" style="font-size: 15px; font-weight: bold"><?php display_message(); ?></span></h2>
    </div>
    <form action="" method="post">
        <div class="row">
            <div class="col-md-3 left-side-edu">
                <h3><b>Help</b></h3>
            </div><!-- end of left-side-edu -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="help" class="col-sm-3 form-control-label">Send Your Problem</label>
                        <div class="col-sm-9">
                            <textarea name="help" id="help" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <input type="submit" name="btn_help" value="Submit" class="btn btn-primary btn-block">
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
    </form>
        <!-- end of education section -->
    <form action="" method="post">
        <div class="row">
            <div class="col-md-3 left-side-home">
                <h3><b>Advice</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="advice" class="col-sm-3 form-control-label">Send Your Advice</label>
                        <div class="col-sm-9">
                            <textarea name="advice" id="advice" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <input type="submit" name="btn_advice" value="Submit" class="btn btn-primary btn-block">
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
    </form>
        <!-- End of Zone You Leave Section -->
</div>