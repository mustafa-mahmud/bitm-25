<?php
include_once ('raw/init.php');

?>

<?php include_once 'asset/include/cover.php'; ?>
<?php include_once 'asset/include/profile_nav.php'; ?>

<div class="container">
    <div class="page-header">
        <h2>ABOUT <a href="about_update.php" title="update" style="text-decoration: none;">Update Information <i class="fa fa-pencil-square-o"></i></a> <?php display_message(); ?></h2>
    </div>
    <form action="" method="post">
        <div class="row">
            <div class="col-md-3 left-side-edu">
                <h3><b>Education</b></h3>
            </div><!-- end of left-side-edu -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="profession" class="col-sm-3 form-control-label">Profession</label>
                        <div class="col-sm-9">
                            <label for="profession" class="form-control-label"><?php echo $query_result['profession']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="schoolName" class="col-sm-3 form-control-label">School Name</label>
                        <div class="col-sm-9">
                            <label id="schoolName" class="form-control-label"><?php echo $query_result['school_name']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="collegeName" class="col-sm-3 form-control-label">College Name</label>
                        <div class="col-sm-9">
                            <label id="collegeName" class="form-control-label"><?php echo $query_result['college_name']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="versityName" class="col-sm-3 form-control-label">University Name</label>
                        <div class="col-sm-9">
                            <label id="versityName" class="form-control-label"><?php echo $query_result['university_name']; ?></label>
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
        <!-- end of education section -->
        <div class="row">
            <div class="col-md-3 left-side-home">
                <h3><b>Zone You Leave</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="currentZone" class="col-sm-3 form-control-label">Current Zone</label>
                        <div class="col-sm-9">
                            <label id="currentZone" class="form-control-label"><?php echo $query_result['currect_zone']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="homeZone" class="col-sm-3 form-control-label">Home Zone</label>
                        <div class="col-sm-9">
                            <label id="homeZone" class="form-control-label"><?php echo $query_result['home_zone']; ?></label>
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
        <!-- End of Zone You Leave Section -->
        <div class="row">
            <div class="col-md-3 left-side-rel">
                <h3><b>Relationship</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="relation" class="col-sm-3 form-control-label">Relationship Status</label>
                        <div class="col-sm-9">
                            <label id="relation" class="form-control-label">
                                <?php
                                    if($query_result['relationship_status'] == 1)
                                    {
                                        echo 'Single';
                                    }
                                    else
                                    {
                                        echo 'Married';
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
        <!-- end of Relationship Status Section -->
        <div class="row">
            <div class="col-md-3 left-side-info">
                <h3><b>Basic Information</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="userName" class="col-sm-3 form-control-label">Username</label>
                        <div class="col-sm-9">
                            <label id="userName" class="form-control-label"><?php echo $query_result['username']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="emailUser" class="col-sm-3 form-control-label">Email</label>
                        <div class="col-sm-9">
                            <label id="emailUser" class="form-control-label"><?php echo $query_result['email']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="userbirthDate" class="col-sm-3 form-control-label">Birth Date</label>
                        <div class="col-sm-9">
                            <label id="userbirthDate" class="form-control-label"><?php echo $query_result['date_of_birth']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="gender" class="col-sm-3 form-control-label">Gender</label>
                        <div class="col-sm-9">
                            <label id="gender" class="form-control-label">
                                <?php
                                    if($query_result['gender'] == 1)
                                    {
                                        echo 'Male';
                                    }
                                    else
                                    {
                                        echo 'Female';
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="religious" class="col-sm-3 form-control-label">Religious</label>
                        <div class="col-sm-9">
                            <label id="religious" class="form-control-label"><?php echo $query_result['religious']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="aboutMe" class="col-sm-3 form-control-label">About Me</label>
                        <div class="col-sm-9">
                            <textarea id="aboutMe" class="form-control" disabled="disabled"><?php echo $query_result['about_me']; ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <a href="about_update.php" class="btn btn-primary btn-block">Update Information</a>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row-->
        <!-- end of Basic Information Section -->
    </form>
</div>