<?php
include_once ('./raw/init.php');
$user_id = $_SESSION['user_id'];

if(isset($_GET['acc_id']))
{
    insert_friend_list($user_id,$_GET['acc_id']);
    delete_from_pending_list($user_id,$_GET['acc_id']);
    redirect("request_friends.php");
}


$result_pending = select_from_pending_list_receiver($user_id);

?>

<?php include_once 'asset/include/cover.php'; ?>
<?php include_once 'asset/include/profile_nav.php'; ?>

<div class="col-md-12">
    <div class="container">
        <div class="col-md-8">
            <div class="row">
                <?php while ($requests = mysqli_fetch_assoc($result_pending)) { ?>
                <?php
                    $user = select_user_by_id($requests['sender_id']);
                ?>
                <div class="col-md-12">
                    <div class="col-md-6" align="center">
                        <a href="user_id.php?member=user&user_id=<?php echo $row['user_id']; ?>"><img src="<?php echo $user['users_avator']; ?>" alt="avatar" class="img-thumbnail" style="width: 150px; height: 150px;"></a>
                        <div class="text-center"><b><?php echo $user['first_name']. ' '.$user['last_name']; ?></b></div>
                    </div>
                    <div class="col-md-6">
                        <p><a href="request_friends.php?acc_id=<?php echo $user['user_id']; ?>" class="btn btn-primary btn-block"><i class="fa fa-user"></i> Accept</a></p>
                        <p><a href="#" class="btn btn-primary btn-block"><i class="fa fa-close"></i> Ignore</a></p>
                    </div>
                </div><!-- end of user one -->
                <?php } ?>
            </div><!-- end of row -->
        </div><!-- end of col-md-4 -->
    </div><!-- end of container -->
</div>