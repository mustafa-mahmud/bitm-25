<?php
include_once ('./raw/init.php');
$user_id = $_SESSION['user_id'];

if(isset($_GET['member']))
{
    if(isset($_GET['user_id']))
    {
        $user_id = $_GET['user_id'];
        if($_GET['member'] == 'user')
        {
            select_all_user_by_user_id($user_id);
        }
    }
}

$firends = select_from_friend_list($user_id);

?>

<?php include_once 'asset/include/cover.php'; ?>
<?php include_once 'asset/include/profile_nav.php'; ?>

<div class="col-md-12">
    <div class="container">
        <?php while ($friends_col = fetch_assoc($firends)){ 
            if($friends_col['first_friend_id'] != $user_id)
            {
                $user = select_user_by_id($friends_col['first_friend_id']);
            }
            if($friends_col['second_friend_id'] != $user_id)
            {
                $user = select_user_by_id($friends_col['second_friend_id']);
            }
            $user = fetch_assoc($user);
        ?>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <a href="user_id.php?member=user&user_id=<?php echo $user['user_id']; ?>"><img src="<?php echo $user['users_avator']; ?>" alt="avatar" class="img-thumbnail" style="width: 150px; height: 150px;"></a>
                        <div class="text-center"><b><?php echo $user['first_name']; ?></b></div>
                    </div>
                    <div class="col-md-6">
                        <p><a href="user_id.php?member=user&user_id=<?php echo $user['user_id']; ?>" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> View Profile</a></p>
                        <p><a href="#" class="btn btn-primary btn-block"><i class="fa fa-envelope"></i> Send Message</a></p>
                    </div>
                </div><!-- end of user one -->
            </div><!-- end of row -->
        </div><!-- end of col-md-4 -->
        <?php } ?>
    </div><!-- end of container -->
</div>