<?php
include_once ('raw/init.php');
$user_id = $_SESSION['user_id'];

$request_list_pending=[];
$result_pending = select_from_pending_list($user_id);
$result_friends = select_from_friend_list($user_id);

$query_result = select_all_user_info_by_user_id($_GET['user_id']);
if(isset($_GET['member']))
{
    if(isset($_GET['user_id']))
    {
        $user_id = $_GET['user_id'];
        if($_GET['member'] == 'user')
        {
            select_all_user_by_user_id($user_id);
        }
    }
}

?>

<?php include_once 'asset/include/cover.php'; ?>

<div class="pro-menu">
    <nav class="navbar navbar-inverse">
        <ul class="nav navbar-nav pro-nav">
            <li style="margin: 6px 0 0 -80px; color: #fff; font-size: 3rem; font-weight: bold; text-shadow: 2px 2px 2px #666;"><?php echo $query_result['first_name']. ' ' .$query_result['last_name']; ?></li>
            <li style="margin-top: 10px; color: #222;">~~~~~</li>
            <li><a href="#" class="btn btn-req-friend">ABOUT</a></li>
            <li><a href="#" class="btn btn-req-friend" style="border-right: 2px solid #111;">PHOTO</a></li>
            <li style="margin-top: 10px; color: #222;">~~~~~</li>
            <li><a href=""?member=user&r_id=<?php echo $query_result['user_id']?>&s_id=<?php echo $_SESSION['user_id']; ?>" class="btn" style="background-color: #000; font-size: 2rem; font-weight: bold; box-shadow: 4px 4px 4px #111; text-shadow: 0.5px 0.5px 1px #fff; color: #fff;">
                    <?php
                        $relation = 0;
                        foreach ($result_pending  as $request)
                        {
                            if($request['reciever_id'] == $query_result['user_id'])
                            {
                                $relation = 1;
                            }
                        }
                                            
                        foreach ($result_friends as $request)
                        {
                            if($request['first_friend_id'] == $query_result['user_id'] || $request['second_friend_id'] == $query_result['user_id'])
                                {
                                    $relation = 2;
                                }
                            }
                                            
                            if($relation == 0)
                            {
                                echo 'Add Friend';
                            }
                            elseif($relation == 1)
                            {
                                echo 'Request Send';
                            }
                            elseif($relation == 2)
                            {
                                echo 'Friend';
                            }
                    ?>
                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                </a></li>
        </ul>
    </nav>
</div><!-- end of pro-menu -->

<div class="container">
    <div class="page-header">
        <h2 style="font-weight: bold;">ABOUT <?php echo $query_result['first_name']. ' ' .$query_result['last_name']; ?></h2>
    </div>
    <form action="" method="post">
        <div class="row">
            <div class="col-md-3 left-side-edu">
                <h3><b>Education</b></h3>
            </div><!-- end of left-side-edu -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="profession" class="col-sm-3 form-control-label">Profession</label>
                        <div class="col-sm-9">
                            <label for="profession" class="form-control-label"><?php echo $query_result['profession']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="schoolName" class="col-sm-3 form-control-label">School Name</label>
                        <div class="col-sm-9">
                            <label id="schoolName" class="form-control-label"><?php echo $query_result['school_name']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="collegeName" class="col-sm-3 form-control-label">College Name</label>
                        <div class="col-sm-9">
                            <label id="collegeName" class="form-control-label"><?php echo $query_result['college_name']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="versityName" class="col-sm-3 form-control-label">University Name</label>
                        <div class="col-sm-9">
                            <label id="versityName" class="form-control-label"><?php echo $query_result['university_name']; ?></label>
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
        <!-- end of education section -->
        <div class="row">
            <div class="col-md-3 left-side-home">
                <h3><b>Zone You Leave</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="currentZone" class="col-sm-3 form-control-label">Current Zone</label>
                        <div class="col-sm-9">
                            <label id="currentZone" class="form-control-label"><?php echo $query_result['currect_zone']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="homeZone" class="col-sm-3 form-control-label">Home Zone</label>
                        <div class="col-sm-9">
                            <label id="homeZone" class="form-control-label"><?php echo $query_result['home_zone']; ?></label>
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
        <!-- End of Zone You Leave Section -->
        <div class="row">
            <div class="col-md-3 left-side-rel">
                <h3><b>Relationship</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="relation" class="col-sm-3 form-control-label">Relationship Status</label>
                        <div class="col-sm-9">
                            <label id="relation" class="form-control-label">
                                <?php
                                    if($query_result['relationship_status'] == 1)
                                    {
                                        echo 'Single';
                                    }
                                    elseif($query_result['relationship_status'] == 2)
                                    {
                                        echo 'Married';
                                    }
                                    else
                                    {
                                        echo 'Select Relationship Status';
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
        <!-- end of Relationship Status Section -->
        <div class="row">
            <div class="col-md-3 left-side-info">
                <h3><b>Basic Information</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="userName" class="col-sm-3 form-control-label">Username</label>
                        <div class="col-sm-9">
                            <label id="userName" class="form-control-label"><?php echo $query_result['username']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="emailUser" class="col-sm-3 form-control-label">Email</label>
                        <div class="col-sm-9">
                            <label id="emailUser" class="form-control-label"><?php echo $query_result['email']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="gender" class="col-sm-3 form-control-label">Gender</label>
                        <div class="col-sm-9">
                            <label id="gender" class="form-control-label">
                                <?php
                                    if($query_result['gender'] == 1)
                                    {
                                        echo 'Male';
                                    }
                                    elseif($query_result['gender'] == 2)
                                    {
                                        echo 'Female';
                                    }
                                    else
                                    {
                                        echo 'Select your gender';
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="religious" class="col-sm-3 form-control-label">Religious</label>
                        <div class="col-sm-9">
                            <label id="religious" class="form-control-label"><?php echo $query_result['religious']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="aboutMe" class="col-sm-3 form-control-label">About Me</label>
                        <div class="col-sm-9">
                            <textarea id="aboutMe" class="form-control" disabled="disabled"><?php echo $query_result['about_me']; ?></textarea>
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row-->
        <!-- end of Basic Information Section -->
    </form>
</div>