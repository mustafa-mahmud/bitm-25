<?php
include_once ('raw/init.php');

if(isset($_POST['btn']))
{
    save_user_info($_POST);
}

?>

<?php include_once 'asset/include/cover.php'; ?>
<?php include_once 'asset/include/profile_nav.php'; ?>

<div class="container">
    <div class="page-header">
        <h2>ABOUT UPDATE</h2>
    </div>
    <form action="" method="post">
        <div class="row">
            <div class="col-md-3 left-side-edu">
                <h3><b>Education</b></h3>
            </div><!-- end of left-side-edu -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="profession" class="col-sm-3 form-control-label">Profession</label>
                        <div class="col-sm-9">
                            <input name="profession" type="text" class="form-control" id="profession">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="schoolName" class="col-sm-3 form-control-label">School Name</label>
                        <div class="col-sm-9">
                            <input name="school_name" type="text" class="form-control" id="schoolName">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="collegeName" class="col-sm-3 form-control-label">College Name</label>
                        <div class="col-sm-9">
                            <input name="college_name" type="text" class="form-control" id="collegeName">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="versityName" class="col-sm-3 form-control-label">University Name</label>
                        <div class="col-sm-9">
                            <input name="university_name" type="text" class="form-control" id="versityName">
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
        <!-- end of education section -->
        <div class="row">
            <div class="col-md-3 left-side-home">
                <h3><b>Zone You Leave</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="currentZone" class="col-sm-3 form-control-label">Current Zone</label>
                        <div class="col-sm-9">
                            <input name="currect_zone" type="text" class="form-control" id="currentZone">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="homeZone" class="col-sm-3 form-control-label">Home Zone</label>
                        <div class="col-sm-9">
                            <input name="home_zone" type="text" class="form-control" id="homeZone">
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
        <!-- End of Zone You Leave Section -->
        <div class="row">
            <div class="col-md-3 left-side-rel">
                <h3><b>Relationship</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="relation" class="col-sm-3 form-control-label">Relationship Status</label>
                        <div class="col-sm-9">
                            <select id="relation" class="form-control" name="relationship_status">
                                <option>Relationship Status</option>
                                <option value="1">Single</option>
                                <option value="2">Married</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row -->
        <!-- end of Relationship Status Section -->
        <div class="row">
            <div class="col-md-3 left-side-info">
                <h3><b>Basic Information</b></h3>
            </div><!-- end of left-side-home -->
            <div class="well col-md-9">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="userName" class="col-sm-3 form-control-label">Username</label>
                        <div class="col-sm-9">
                            <input name="username" type="text" class="form-control" id="userName">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="emailUser" class="col-sm-3 form-control-label">Email</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="emailUser">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="userbirthDate" class="col-sm-3 form-control-label">Birth Date</label>
                        <div class="col-sm-9">
                            <input name="date_of_birth" type="date_of_birth" class="form-control" id="userbirthDate">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="gender" class="col-sm-3 form-control-label">Gender</label>
                        <div class="col-sm-9">
                            <select id="relation" class="form-control" name="gender">
                                <option>Gender</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="religious" class="col-sm-3 form-control-label">Religious</label>
                        <div class="col-sm-9">
                            <input name="religious" type="text" class="form-control" id="religious">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="aboutMe" class="col-sm-3 form-control-label">About Me</label>
                        <div class="col-sm-9">
                            <textarea name="about_me" id="aboutMe" class="form-control"></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <input type="submit" name="btn" value="Save Information" class="btn btn-primary btn-block">
                    </div>
                </div>
            </div><!-- end of col-md-9 -->
        </div><!-- end of row-->
        <!-- end of Basic Information Section -->
    </form>
</div>