<?php
include_once ('./raw/init.php');
$user_id = $_SESSION['user_id'];

$firends = select_from_friend_list_limit($user_id);

if(isset($_GET['member']))
{
    if(isset($_GET['user_id']))
    {
        $user_id = $_GET['user_id'];
        if($_GET['member'] == 'user')
        {
            select_all_user_by_user_id($user_id);
        }
    }
}



?>

<?php include_once 'asset/include/cover.php'; ?>
<?php include_once 'asset/include/profile_nav.php'; ?>


<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">WHO I AM</h3>
        </div>
        <div class="panel-body descrive">
            <textarea disabled="disabled"><?php echo $query_result['about_me']; ?></textarea>
        </div>
    </div> <!-- end of describe -->
    <div class="panel panel-default firends">
        <div class="panel-heading">
            <h3 class="panel-title"><b>Friends</b></h3>
        </div>
        <div class="panel-body">
            <ul class="list-inline">
                <?php
                    while ($friends_col = fetch_assoc($firends)){
                    if($friends_col['first_friend_id'] != $user_id)
                    {
                        $user = select_user_by_id($friends_col['first_friend_id']);
                    }
                    if($friends_col['second_friend_id'] != $user_id)
                    {
                        $user = select_user_by_id($friends_col['second_friend_id']);
                    }
                    $user = fetch_assoc($user);
                ?>
                <li><a href="user_id.php?member=user&user_id=<?php echo $user['user_id']; ?>" class="thumbnail"><img src="<?php echo $user['users_avator']; ?>" alt="<?php echo $user['username']; ?>" style="width: 65px; height: 65px;"></a></li>
                <?php } ?>
            </ul>
            <div class="clearfix"></div>
            <a href="my_friends.php" class="btn btn-primary btn-block">View All Friends</a>
        </div>
    </div><!-- end of add friends-->
</div><!-- end of col-md-3 -->
<div class="col-md-8">
    <div class="col-md-10">
        <?php include_once ('status.php'); ?>
        <?php while ($row = fetch_assoc($status_result)) { ?>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">
                        <a href="user_id.php?member=user&user_id=<?php echo $row['user_id']; ?>" class="post-avator thumbnail">
                            <img src="<?php
                            $users = select_user_by_id($row['user_id']);
                            $user = fetch_assoc($users);
                            echo $user['users_avator']; ?>" style="width: 64px; height: 64px;"/>
                            <div class="text-center"><?php echo $user['first_name']; ?></div>
                        </a>
                        <!-- show like -->
                        <div class="text-center"><i class="fa fa-thumbs-o-up"></i> <?php echo $row['status_likes']; ?></div>
                        <!-- show like -->
                    </div><!-- end of col-sm-2 -->
                    <div class="col-sm-10">
                        <div class="pointer well">
                            
                            <p class="text-justify"><?php echo $row['status_description']; ?></p>
                            <?php
                                if($row['status_images'] != NULL)
                                {
                                    echo "<img src=\"".  $row['status_images']. "\"" 
                                    . "alt=\"". "NO --- Image". "\"". "class=\"img-thumbnail\" style=\"width: 450px; height:350px;\">";
                                }
                            ?>
                        </div>
                        <p class="post-action"><a href="profile.php?status_id=<?php echo $row['status_id']; ?>">Like</a> - <a href="user_id.php?member=user&user_id=<?php echo $user['user_id']; ?>">Comment</a>
                        <div class="comment-form">
                            <form class="form-inline" action="" method="post" act="">
                                <div class="form-group">
                                    <input type="text" name="comment_description" placeholder="Comment" class="form-control" />
                                    <input type="submit" value="Comment" name="btn_comment" class="btn btn-default form-control" />
                                    <input type="hidden" value="<?php echo $row['status_id']; ?>" name="id" />
                                </div>
                            </form>
                        </div><!-- end of comment -->
                        <div class="clearfix"></div>
                            <?php $comment_result = select_all_comment_by_comment_id($row['status_id']);
                            while ($row = fetch_assoc($comment_result)) { ?>
                            <div class="comment">
                                <a href="user_id.php?member=user&user_id=<?php echo $row['user_id']; ?>" class="comment-avator pull-left">
                                    <img src="<?php
                                    $users_comment = select_user_by_id($row['user_id']);
                                    $user_comment = fetch_assoc($users_comment);
                                    echo $user_comment['users_avator']; ?>" alt="<?php echo $user_comment['first_name']; ?>" style="width: 28px; height: 28px;" class="img-rounded" />
                                </a>
                                    <div class="comment-text">
                                    <p><?php echo $row['comment_description']; ?></p>
                                </div>
                            </div><!-- end of comment view -->
                            <?php }?>
                        <div class="clearfix"></div> 
                    </div><!-- end of col-sm-8 -->
                </div><!-- end of row -->
            </div><!-- end of plane-body -->
        </div><!-- end of post -->
        <?php } ?>
    </div><!-- end of col-md-10 -->
</div>