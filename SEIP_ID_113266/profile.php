<?php
include_once ('./raw/init.php');

if(isset($_GET['status_id']))
{
    increase_likes($_GET['status_id']);
    redirect('profile.php');
}


$query_result = select_all_user_info();

    if(isset($_GET['member']))
    {
        if(isset($_GET['user_id']))
        {
            $user_id = $_GET['user_id'];
            if($_GET['member'] == 'user')
            {
                select_all_user_by_user_id($user_id);
            }
        }
    }
    
    elseif(isset($_POST['btn']))
    {
        upload_avatar($_POST,$_FILES);
    }
    
    if(isset($_POST['btn']))
    {
        upload_cover_photo($_POST, $_FILES);
    }
    
    if(isset($_POST['status_bnt']))
    {
        give_status($_POST,$_FILES);
    }
    
    $status_result = select_all_status_by_user_id();
    
    if(isset($_POST['btn_comment']))
    {
        give_comment($_POST);
    }
    
    $user_id = $_SESSION['user_id'];
    if($user_id == NULL)
    {
        redirect("index.php");
    }

    if(isset($_GET['status']))
    {
        user_logout();
    }
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Profile</title>
        <?php include_once ('asset/include/header.php'); ?>
    </head>
    <body>
        <?php include_once ('./asset/include/nav.php'); ?>
        
        <div class="clearfix"></div>
        <section>
            <!-- cover photo -->
            <!-- profile navigation -->
            
            <?php
                if(isset($pages))
                {
                    if($pages == 'about')
                    {
                        include_once ('pages/about_content.php');
                    }
                    elseif($pages == 'about_update')
                    {
                        include_once ('pages/about_update_content.php');
                    }
                    elseif($pages == 'my_friends')
                    {
                    include_once ('pages/my_friends_content.php');
                    }
                    elseif ($pages == 'my_photos')
                    {
                        include_once ('pages/my_photos_content.php');
                    }
                    elseif($pages == 'user_id')
                    {
                        include_once './pages/user_id_content.php';
                    }
                    elseif($pages == 'request_friend')
                    {
                        include_once ('./pages/request_friend_content.php');
                    }
                    elseif($pages == 'setting')
                    {
                        include_once ('pages/setting_content.php');
                    }
                    elseif($pages == 'advice')
                    {
                        include_once ('./pages/advice_content.php');
                    }
                }
                else
                {
                    include_once ('pages/profile_content.php');
                }
            
            
            ?>
            
        </section><!-- end of section -->
        <!-- JAVASCRIPT -->
        <?php include_once ('asset/include/footer.php'); ?>
        
        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#show_img')
                            .attr('src', e.target.result)
                            .width(144)
                            .height(144);
                };

                reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
    </body>
</html>
