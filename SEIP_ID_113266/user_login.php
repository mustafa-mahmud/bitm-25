<div class="col-md-6 right">
    
    <form action="" method="post" onsubmit="return validateStandard(this)">
        <div class="form-group col-md-10">
            <input type="email" name="email" placeholder="YOUR EMAIL" class="form-control" required="required" regexp="JSVAL_RX_EMAIL" err="Please Enter Valied Email Address">
        </div>
        <div class="form-group col-md-10">
            <input type="password" name="password" placeholder="PASSWORD" class="form-control" required="required">
        </div>
        <div class="form-group col-md-10">
            <label><input type="checkbox" name="remember" placeholder="PASSWORD"> Remember</label>
        </div>
        <div class="form-group col-md-10">
            <input type="submit" name="btn" value="SIGN IN" class="form-control btn btn-default btn-block">
            <a href="recover_password.php" style="font-size: 15px; color: #093; text-decoration: underline;" class="pull-right">forget password</a>
        </div>
    </form>
    <div class="col-md-12">
        <textarea style="color:#fff; font-size: 1.5rem;" class="col-md-10 text-justify" readonly><?php display_message(); validate_user_registration(); validate_user_login(); ?></textarea>
    </div>
</div>