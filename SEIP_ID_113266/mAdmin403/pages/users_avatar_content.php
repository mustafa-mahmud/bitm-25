<?php

$result = $obj_msoul_users_info->select_all_msoul_user_info();

?>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Users Status</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="min-tablet">F + L Name</th>
                    <th class="min-tablet">Username</th>
                    <th class="min-tablet">User Avatar</th>
                    <th class="min-tablet">Cover Photo</th>
                    <th class="min-tablet">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; while ($status_result = mysqli_fetch_assoc($result)) { ?>
                <tr align="center">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $status_result['first_name']. ' ' .$status_result['last_name']; ?></td>
                    <td><?php echo $status_result['username']; ?></td>
                    <td><img src="../<?php echo $status_result['users_avator']; ?>" style="width: 150px; height: 100px;" class="img-thumbnail"></td>
                    <td><img src="../<?php echo $status_result['cover_photo']; ?>" style="width: 150px; height: 100px;" class="img-thumbnail"></td>
                    <td><a href="#" style="text-decoration: none; background: red; padding: 4px; color: #fff;">Remove</a></td>
                </tr>
                <?php $i++; } ?>
            </tbody>
        </table>
    </div>
</div>