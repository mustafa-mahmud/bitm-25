<?php

    $result = $obj_admin->select_category_info_by_id($category_id);
    $category_info = mysqli_fetch_assoc($result);

    if(isset($_POST['btn']))
    {
        $obj_admin->update_category_info($_POST);
    }

?>

<div id="page-title">
    <h1 class="page-header text-overflow">Category</h1>

    <!--Searchbox-->
    <div class="searchbox">
        <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search..">
            <span class="input-group-btn">
                <button class="text-muted" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->


<!--Breadcrumb-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<ol class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Category</a></li>
    <li class="active">Update</li>
</ol>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->

<!-------------------------ADD CATEGORY CONTENT-------------------------------->

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Update Category</h3>
    </div>


    <!-- BASIC FORM ELEMENTS -->
    <!--===================================================-->
    <form class="panel-body form-horizontal form-padding" action="" method="post" name="edit_category">

        <!--Text Input-->
        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-text-input">Category Name</label>
            <div class="col-md-9">
                <input name="category_name" value="<?php echo $category_info['category_name']; ?>" type="text" id="demo-text-input" class="form-control" placeholder="category name">
                <input name="category_id" value="<?php echo $category_info['category_id']; ?>" type="hidden">
            </div>
        </div>

        <!--Textarea-->
        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input">Publication Status</label>
            <div class="col-md-9">
                <textarea name="category_description" id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."><?php echo $category_info['category_description']; ?></textarea>
            </div>
        </div>

        <label class="col-md-3 control-label" for="demo-textarea-input">Publication Status</label>
        <div class="col-md-9">
            <select class="selectpicker" name="publication_status">
                <option>Select Publication Status</option>
                <option value="1">Published</option>
                <option value="2">Unpublished</option>
            </select>
        </div>
        
        <div class="clearfix"></div>
        <br>
        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input"></label>
            <div class="col-md-9">
                <input type="submit" value="Update Category" name="btn" class="btn btn-primary btn-block">
            </div>
        </div>

    </form>
</div>
<!--===================================================-->

<script type="text/javascript">
    document.forms['edit_category'].elements['publication_status'].value = '<?php echo $category_info['publication_status'] ?>';
</script>