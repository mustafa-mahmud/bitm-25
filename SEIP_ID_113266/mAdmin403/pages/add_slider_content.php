<?php

    if(isset($_POST['btn']))
    {
        $message = $obj_admin->add_slider($_POST,$_FILES);
    }

?>

<div id="page-title">
    <h1 class="page-header text-overflow">Slider</h1>

    <!--Searchbox-->
    <div class="searchbox">
        <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search..">
            <span class="input-group-btn">
                <button class="text-muted" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->


<!--Breadcrumb-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<ol class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Slider</a></li>
    <li class="active">Add</li>
</ol>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->

<!-------------------------ADD CATEGORY CONTENT-------------------------------->

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Add Slider <span class="text-success" style="font-size: 15px; font-weight: bold;">
            <?php if(isset($message)) { echo $message; unset($message); } ?> </span></h3>
    </div>


    <!-- BASIC FORM ELEMENTS -->
    <!--===================================================-->
    <form class="panel-body form-horizontal form-padding" action="" method="post" enctype="multipart/form-data">

        <!--Text Input-->
        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-text-input">Add Slider</label>
            <div class="col-md-9">
                <input name="slider_name" type="text" id="demo-text-input" class="form-control" placeholder="Slider Name">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Slider Image</label>
            <div class="col-md-9">
                <span class="btn btn-default btn-file">
                    Select Image... <input type="file" name="slider_image">
                </span>
            </div>
        </div>


        <label class="col-md-3 control-label" for="demo-textarea-input">Publication Status</label>
        <div class="col-md-9">
            <select class="selectpicker" name="publication_status">
                <option>Select Publication Status</option>
                <option value="1">Published</option>
                <option value="2">Unpublished</option>
            </select>
        </div>
        
        <div class="clearfix"></div>
        <br>
        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input"></label>
            <div class="col-md-9">
                <input type="submit" value="Save Manufacturer" name="btn" class="btn btn-primary btn-block">
            </div>
        </div>
   </form>     
</div>