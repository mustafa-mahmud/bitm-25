<?php

$result = $obj_msoul_users_info->select_all_msoul_user_info();

if(isset($_GET['status']))
{
    $status = $_GET['status'];
    $user_id = $_GET['user_id'];
    if($status == 'active')
    {
        $obj_msoul_users_info->user_activity_uncheck($user_id);
    }
    elseif($status == 'inactive')
    {
        $obj_msoul_users_info->user_activity_check($user_id);
    }
    elseif($status == 'delete')
    {
        $obj_msoul_users_info->delete_user($user_id);
    }
}


?>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">User Manage</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="min-tablet">F + L Name</th>
                    <th class="min-tablet">Users Avatar</th>
                    <th class="min-tablet">Email</th>
                    <th class="min-tablet">Birth Date</th>
                    <th class="min-tablet">Gender</th>
                    <th class="min-tablet">Religious</th>
                    <th class="min-tablet">Active</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; while ($user_res = mysqli_fetch_assoc($result)) { ?>
                <tr align="center">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $user_res['first_name']. ' ' .$user_res['last_name']; ?></td>
                    <td><?php echo $user_res['username']; ?></td>
                    <td><?php echo $user_res['email']; ?></td>
                    <td><?php echo $user_res['date_of_birth']; ?></td>
                    <td>
                        <?php
                            if($user_res['gender'] == 1)
                            {
                                echo 'Male';
                            }
                            else
                            {
                                echo 'Female';
                            }
                        ?>
                    </td>
                    <td><?php echo $user_res['religious']; ?></td>
                    <td>
                        <?php if($user_res['active'] == 1) { ?>
                            
                        <a href="?status=active&user_id=<?php echo $user_res['user_id']; ?>" style="text-decoration: none; background-color: green; padding:4px; color: #fff;" >Active </a>
                            
                        <?php } else { ?>
                            
                        <a href="?status=inactive&user_id=<?php echo $user_res['user_id']; ?>" style="text-decoration: none; background-color: red; padding:4px; color: #fff;" >Inactive </a>
                            
                        <?php } ?>
                        <a href="?status=delete&user_id=<?php echo $user_res['user_id']; ?>" onclick="return check_delete();" style="text-decoration: none; background-color: red; padding:4px; color: #fff; margin-left: 2px;" title="delete">Delete</a>
                    </td>
                </tr>
                <?php $i++; } ?>
            </tbody>
        </table>
    </div>
</div>