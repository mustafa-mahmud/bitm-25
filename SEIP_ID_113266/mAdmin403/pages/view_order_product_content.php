<?php
$order_id = $_GET['order_id'];

$query_result = $obj_cart->select_customer_info_by_order_id($order_id);
$customer_info = mysqli_fetch_assoc($query_result);

$query_res = $obj_cart->select_product_info_by_order_id($order_id);

?>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Customer Information</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Order ID</th>
                    <th class="min-tablet">Customer Name</th>
                    <th class="min-tablet">Email Address</th>
                    <th class="min-tablet">Address</th>
                    <th class="min-tablet">Phone Number</th>
                    <th class="min-tablet">City</th>
                    <th class="min-tablet">Country</th>
                </tr>
            </thead>
            <tbody>
                <!--------- ------------>
                <tr>
                    <td><?php echo $customer_info['order_id']; ?></td>
                    <td><?php echo $customer_info['first_name']. ' '.$customer_info['last_name']; ?></td>
                    <td><?php echo $customer_info['email']; ?></td>
                    <td><?php echo $customer_info['address']; ?></td>
                    <td><?php echo $customer_info['phone_number']; ?></td>
                    <td><?php echo $customer_info['city']; ?></td>
                    <td><?php echo $customer_info['country']; ?></td>
                </tr>
                <!--------- ------------>
            </tbody>
        </table>
    </div>
</div>


<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Shipping Information</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Order ID</th>
                    <th class="min-tablet">Customer Name</th>
                    <th class="min-tablet">Email Address</th>
                    <th class="min-tablet">Address</th>
                    <th class="min-tablet">Phone Number</th>
                    <th class="min-tablet">City</th>
                    <th class="min-tablet">Country</th>
                    <th class="min-tablet">Zip Code</th>
                </tr>
            </thead>
            <tbody>
                <!--------- ------------>
                <tr>
                    <td><?php echo $customer_info['order_id']; ?></td>
                    <td><?php echo $customer_info['full_name']; ?></td>
                    <td><?php echo $customer_info['email_address']; ?></td>
                    <td><?php echo $customer_info['address']; ?></td>
                    <td><?php echo $customer_info['phone_number']; ?></td>
                    <td><?php echo $customer_info['city']; ?></td>
                    <td><?php echo $customer_info['country']; ?></td>
                    <td><?php echo $customer_info['zip_code']; ?></td>
                </tr>
                <!--------- ------------>
            </tbody>
        </table>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Product Information</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="min-tablet">Product ID</th>
                    <th class="min-tablet">Product Name</th>
                    <th class="min-tablet">Product Image</th>
                    <th class="min-tablet">Product Price</th>
                    <th class="min-tablet">product Sale Quantity</th>
                    <th class="min-tablet">Total Price</th>
                </tr>
            </thead>
            <tbody>
                <?php $sum = 0; while ($product_info = mysqli_fetch_assoc($query_res)) { ?>
                <tr align="center">
                    <td><?php echo $product_info['proudct_id']; ?></td>
                    <td><?php echo $product_info['product_name']; ?></td>
                    <td><img src="<?php echo $product_info['product_image']; ?>" style="width: 150px; height: 100px;" class="img-thumbnail"></td>
                    <td><?php echo $product_info['product_price']; ?></td>
                    <td><?php echo $product_info['product_sales_quantity']; ?></td>
                    <td>BDT 
                        <?php
                            $total = $product_info['order_total'] * $product_info['product_sales_quantity'];
                            echo $total;
                        ?>
                    </td>
                </tr> 
                <?php $sum = $sum + $total; } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Total Sell</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="min-tablet">Total</th>
                    <th class="min-tablet">VAT</th>
                    <th class="min-tablet">Grand Total</th>
                </tr>
            </thead>
            <tbody>
                <tr align="center" style="font-weight: bold;">
                    <td><?php echo $sum; ?></td>
                    <td>BDT <?php $vat = $sum * 4.5; echo $vat; ?></td>
                    <td>BDT <?php echo $sum + $vat; ?></td>
                </tr>
            </tbody> 
        </table>
    </div>
</div>