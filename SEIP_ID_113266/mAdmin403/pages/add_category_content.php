<script type="text/javascript">
//Create a boolean variable to check for a valid Internet Explorer instance.
    var xmlhttp = false;
    //Check if we are using IE.
    try {
        //If the Javascript version is greater than 5.
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        //alert(xmlhttp);
        //alert ("You are using Microsoft Internet Explorer.");
    } catch (e) {
        // alert(e);

        //If not, then use the older active x object.
        try {
            //If we are using Internet Explorer.
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            //alert ("You are using Microsoft Internet Explorer");
        } catch (E) {
            //Else we must be using a non-IE browser.
            xmlhttp = false;
        }
    }
    //If we are using a non-IE browser, create a javascript instance of the object.
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
        //alert ("You are not using Microsoft Internet Explorer");
    }

    function makerequest(given_text, objID)
    {
        //alert(given_text);
        //var obj = document.getElementById(objID);
        serverPage = 'check_category.php?text=' + given_text;
        xmlhttp.open("GET", serverPage);
        xmlhttp.onreadystatechange = function ()
        {
            //alert(xmlhttp.readyState);
            //alert(xmlhttp.status);
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200)
            {
                //alert(xmlhttp.responseText);
                document.getElementById(objID).innerHTML = xmlhttp.responseText;
                //document.getElementById(objcw).innerHTML = xmlhttp.responseText;
                if(xmlhttp.responseText === 'Allready Exists')
                {
                    document.getElementById('btn').disabled = true;
                }
                else
                {
                    document.getElementById('btn').disabled = false;
                }
            }
        }
        xmlhttp.send(null);
    }
</script>

<?php

    if(isset($_POST['btn']))
    {
        $message = $obj_admin->save_category_info($_POST);
    }

?>

<div id="page-title">
    <h1 class="page-header text-overflow">Category</h1>

    <!--Searchbox-->
    <div class="searchbox">
        <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search..">
            <span class="input-group-btn">
                <button class="text-muted" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->


<!--Breadcrumb-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<ol class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Category</a></li>
    <li class="active">Add</li>
</ol>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->

<!-------------------------ADD CATEGORY CONTENT-------------------------------->

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Add Category  <span class="text-success" style="font-size: 15px; font-weight: bold;">
                        <?php
                            if(isset($message))
                            {
                                echo $message;
                                unset($message);
                            }
                        ?>
                </span></h3>
    </div>


    <!-- BASIC FORM ELEMENTS -->
    <!--===================================================-->
    <form class="panel-body form-horizontal form-padding" action="" method="post" onsubmit="return validateStandard(this)">

        <!--Text Input-->
        <div class="form-group"> 
            <label class="col-md-3 control-label" for="demo-text-input">Category Name <span class="required"">*</span></label>
            <div class="col-md-9">
                <span id="res"></span>
                <input name="category_name" type="text" id="demo-text-input" class="form-control" placeholder="category name" required="required" err="Please Enter Valied Category Name" regexp="JSVAL_RX_ALPHA" onblur="return makerequest(this.value,'res')" />
            </div>
        </div>

        <!--Textarea-->
        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input">Category Description</label>
            <div class="col-md-9">
                <textarea name="category_description" id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
            </div>
        </div>

        <label class="col-md-3 control-label" for="demo-textarea-input">Publication Status <span class="required">*</span></label>
        <div class="col-md-9">
            <select class="selectpicker" name="publication_status" required="required" exclude=" ">
                <option value=" ">Select Publication Status</option>
                <option value="1">Published</option>
                <option value="2">Unpublished</option>
            </select>
        </div>
        
        <div class="clearfix"></div>
        <br>
        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input"></label>
            <div class="col-md-9">
                <input type="submit" value="Save Category" name="btn" class="btn btn-primary btn-block">
            </div>
        </div>

    </form>
</div>
<!--===================================================-->