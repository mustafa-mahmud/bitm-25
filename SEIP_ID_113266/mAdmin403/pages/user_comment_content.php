<?php
require_once ('../classes/msoul.php');
$obj_msoul_users_info = new Msoul();

$result = $obj_msoul_users_info->select_users_comment();

if(isset($_GET['status']))
{
    $status = $_GET['status'];
    $comment_id = $_GET['comment_id'];
    if($status == 'delete')
    {
        $obj_msoul_users_info->delete_comment_by_comment($comment_id);
    }
}

?>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Users Status</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="min-tablet">F + L Name</th>
                    <th class="min-tablet">Username</th>
                    <th class="min-tablet">Comment</th>
                    <th class="min-tablet">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; while ($status_result = mysqli_fetch_assoc($result)) { ?>
                <tr align="center">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $status_result['first_name']. ' ' .$status_result['last_name']; ?></td>
                    <td><?php echo $status_result['username']; ?></td>
                    <td><?php echo $status_result['comment_description']; ?></td>
                    <td><a href="?status=delete&comment_id=<?php echo $status_result['comment_id']; ?>" style="text-decoration: none; background: red; padding: 4px; color: #fff;">Remove</a></td>
                </tr>
                <?php $i++; } ?>
            </tbody>
        </table>
    </div>
</div>