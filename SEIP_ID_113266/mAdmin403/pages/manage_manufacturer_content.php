<?php

    $query_result = $obj_admin->select_all_manufacturer();
    
    if(isset($_GET['status']))
    {
        $status = $_GET['status'];
        $id = $_GET['id'];
        if($status == 'active')
        {
            $obj_admin->unpublished_manufacturer($id);
        }
        elseif($status == 'inactive')
        {
            $obj_admin->published_manufacturer($id);
        }
        elseif($status == 'delete')
        {
            $obj_admin->delete_manufacturer($id);
        }
    }


?>

<div id="page-title">
    <h1 class="page-header text-overflow">Manufacturer</h1>

    <!--Searchbox-->
    <div class="searchbox">
        <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search..">
            <span class="input-group-btn">
                <button class="text-muted" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->


<!--Breadcrumb-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<ol class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Manufacturer</a></li>
    <li class="active">Manage</li>
</ol>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->

<!-------------------------MANAGE CATEGORY CONTENT-------------------------------->

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Manufacturer Category</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Manufacturer Name</th>
                    <th class="min-tablet">Image</th>
                    <th class="min-tablet">Publication Status</th>
                    <th class="min-tablet">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = mysqli_fetch_assoc($query_result)) { ?>
                <tr>
                    <td><?php echo $row['manufacturer_id']; ?></td>
                    <td><?php echo $row['manufacturer_name']; ?></td>
                    <td><img src="<?php echo $row['manufacturer_image']; ?>" alt="Manufacturer Image" width="100px" height="100px"></td>
                    <td class="center">
                        <span>
                            <?php
                                if($row['publication_status'] == 1)
                                {
                                    echo "<span style='background-color:green; color: #fff; font-weight: bold; padding: 4px'>Published</span>";
                                }
                                else
                                {
                                    echo "<span style='background-color:red; color: #fff; font-weight: bold; padding: 4px'>Unpublished</span>";
                                }
                            ?>
                        </span>
                    </td>
                    <td style="font-size: 20px">
                        <?php if($row['publication_status'] == 1 ) {?>
                            <a href="?status=active&id=<?php echo $row['manufacturer_id']; ?>" style="color: red;" title="do unpublished"><i class="fa fa-hand-o-up" aria-hidden="true"></i></a>
                        <?php } else { ?>
                            <a href="?status=inactive&id=<?php echo $row['manufacturer_id']; ?>" style="color: green;" title="do published"><i class="fa fa-hand-o-down" aria-hidden="true"></i></a>
                        <?php } ?>
                            <a href="edit_manufacturer.php?id=<?php echo $row['manufacturer_id']; ?>" style="color: #039;" title="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="?status=delete&id=<?php echo $row['manufacturer_id']; ?>" onclick="return check_delete();" style="color: red;" title="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>