<?php

    $category_result = $obj_admin -> select_published_category();
    $manufacturer_result = $obj_admin->select_published_manufacturer();

    if(isset($_POST['btn']))
    {
        $message = $obj_admin->save_product_info($_POST,$_FILES);
    }


?>

<div id="page-title">
    <h1 class="page-header text-overflow">Product</h1>

    <!--Searchbox-->
    <div class="searchbox">
        <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search..">
            <span class="input-group-btn">
                <button class="text-muted" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->


<!--Breadcrumb-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<ol class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Product</a></li>
    <li class="active">Add</li>
</ol>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->

<!-------------------------ADD CATEGORY CONTENT-------------------------------->

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Add Product  <span class="text-success" style="font-size: 15px; font-weight: bold;">
            <?php if(isset($message)){ echo $message; unset($message); } ?>
            </span></h3>
    </div>


    <!-- BASIC FORM ELEMENTS -->
    <!--===================================================-->
    <form class="panel-body form-horizontal form-padding" action="" method="post" enctype="multipart/form-data">

        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-text-input">Product Name</label>
            <div class="col-md-9">
                <input name="product_name" type="text" id="demo-text-input" class="form-control" placeholder="product name">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input">Category Name</label>
            <div class="col-md-9">
                <select class="selectpicker form-control" name="category_id">
                    <option>Select Category Name</option>
                    <?php while ($row = mysqli_fetch_assoc($category_result)) { ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input">Manufacturer Name</label>
            <div class="col-md-9">
                <select class="selectpicker form-control" name="manufacturer_id">
                    <option>Select Manufacturer Name</option>
                    <?php while ($row = mysqli_fetch_array($manufacturer_result)) { ?>
                    <option value="<?php echo $row['manufacturer_id']; ?>"><?php echo $row['manufacturer_name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-text-input">Product Price</label>
            <div class="col-md-9">
                <input type="number" id="demo-text-input" class="form-control" name="product_price">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-text-input">Product Quantity</label>
            <div class="col-md-9">
                <input type="number" id="demo-text-input" class="form-control" name="product_quantity">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-text-input">Product SKU</label>
            <div class="col-md-9">
                <input type="text" id="demo-text-input" class="form-control" name="product_sku">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input">Product Short Description</label>
            <div class="col-md-9">
                <textarea name="product_short_description" id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input">Product Long Description</label>
            <div class="col-md-9">
                <textarea name="product_long_description" id="demo-textarea-input" rows="9" class="form-control" placeholder="Your content here.."></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Product Image</label>
            <div class="col-md-9">
                <span class="btn btn-default btn-file">
                    Select Image... <input type="file" name="product_image">
                </span>
            </div>
        </div>

        <label class="col-md-3 control-label" for="demo-textarea-input">Publication Status</label>
        <div class="col-md-9">
            <select class="selectpicker" name="publication_status">
                <option>Select Publication Status</option>
                <option value="1">Published</option>
                <option value="2">Unpublished</option>
            </select>
        </div>

        <div class="clearfix"></div>
        <br>
        <div class="form-group">
            <label class="col-md-3 control-label" for="demo-textarea-input"></label>
            <div class="col-md-9">
                <input type="submit" value="Save Category" name="btn" class="btn btn-primary btn-block">
            </div>
        </div>

    </form>
</div>
<!--===================================================-->