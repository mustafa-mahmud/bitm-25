<?php

$result = $obj_msoul_users_info->select_all_msoul_user_info();

?>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">User Advice and Help Message</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Order ID</th>
                    <th class="min-tablet">Username</th>
                    <th class="min-tablet">User Problem</th>
                    <th class="min-tablet">User Advice</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; while ($user_res = mysqli_fetch_assoc($result)) { ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $user_res['username']; ?></td>
                    <td><?php echo $user_res['help']; ?></td>
                    <td><?php echo $user_res['advice']; ?></td>
                </tr>
                <?php $i++; } ?>
            </tbody>
        </table>
    </div>
</div>