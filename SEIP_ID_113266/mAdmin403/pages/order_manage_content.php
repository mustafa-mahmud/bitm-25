<?php

$query_result = $obj_cart->select_all_order_info();

?>

<div id="page-title">
    <h1 class="page-header text-overflow">Shipping Order</h1>

    <!--Searchbox-->
    <div class="searchbox">
        <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search..">
            <span class="input-group-btn">
                <button class="text-muted" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->


<!--Breadcrumb-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<ol class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Order</a></li>
    <li class="active">Manage</li>
</ol>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->

<!-------------------------MANAGE CATEGORY CONTENT-------------------------------->

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Order Manage</h3>
    </div>
    <div class="panel-body">
        <table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th class="min-tablet">Order Total</th>
                    <th class="min-tablet">Order Status</th>
                    <th class="min-tablet">Payment Status</th>
                    <th class="min-tablet">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($order_info = mysqli_fetch_assoc($query_result)) { ?>
                <tr>
                    <td><?php echo $order_info['order_id']; ?></td>
                    <td><?php echo $order_info['first_name']. ' ' .$order_info['last_name']; ?></td>
                    <td><?php echo $order_info['order_total']; ?></td>
                    <td><?php echo $order_info['order_status']; ?></td>
                    <td class="center"><?php echo $order_info['payment_status']; ?></td>
                    <td style="font-size: 20px">
                        <a href="view_order_product.php?order_id=<?php echo $order_info['order_id']; ?>" class="text-dark" title="view order"><i class="fa fa-play-circle" aria-hidden="true"></i></a>
                        <a href="#" class="text-success" title="view Invoice"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                        <a href="#" class="text-success" title="Download Invoice"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                        <a href="#" class="text-info" title="Edit order"><i class="fa fa-edit" aria-hidden="true"></i></a>
                        <a href="#" onclick="return check_delete();" style="color: red;" title="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>