<?php
ob_start();
session_start();

require_once '../classes/admin.php';
$obj_admin = new Admin();

require_once ('../classes/cart.php');
$obj_cart = new Cart();

require_once ('../classes/msoul.php');
$obj_msoul_users_info = new Msoul();

if ($_SESSION['admin_id'] == NULL) {
    header('Location: index.php');
}

if (isset($_GET['l_id']) == 'logout') {
    $obj_admin->logout();
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Mirrored from www.themeon.net/nifty/v2.2/dashboard.php by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:43:12 GMT -->
        <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Dashboard | ManSoul</title>


        <!--STYLESHEET-->
        <!--=================================================-->

        <!--Open Sans Font [ OPTIONAL ] -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


        <!--Bootstrap Stylesheet [ REQUIRED ]-->
        <link href="../asset/admin/css/bootstrap.min.css" rel="stylesheet">


        <!--Nifty Stylesheet [ REQUIRED ]-->
        <link href="../asset/admin/css/nifty.min.css" rel="stylesheet">


        <!--Font Awesome [ OPTIONAL ]-->
        <link href="../asset/admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">


        <!--Animate.css [ OPTIONAL ]-->
        <link href="../asset/admin/plugins/animate-css/animate.min.css" rel="stylesheet">


        <!--Morris.js [ OPTIONAL ]-->
        <link href="../asset/admin/plugins/morris-../asset/admin/js/morris.min.css" rel="stylesheet">


        <!--Switchery [ OPTIONAL ]-->
        <link href="../asset/admin/plugins/switchery/switchery.min.css" rel="stylesheet">


        <!--Bootstrap Select [ OPTIONAL ]-->
        <link href="../asset/admin/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">


        <!--Demo script [ DEMONSTRATION ]-->
        <link href="../asset/admin/css/demo/nifty-demo.min.css" rel="stylesheet">
        
        
        <!--myStyle script [ Custom ]-->
        <link href="../asset/admin/css/myStyle.css" rel="stylesheet">




        <!--SCRIPT-->
        <!--=================================================-->

        <!--Page Load Progress Bar [ OPTIONAL ]-->
        <link href="../asset/admin/plugins/pace/pace.min.css" rel="stylesheet">
        <script src="../asset/admin/plugins/pace/pace.min.js"></script>



        <!--

        REQUIRED
        You must include this in your project.

        RECOMMENDED
        This category must be included but you may modify which plugins or components which should be included in your project.

        OPTIONAL
        Optional plugins. You may choose whether to include it in your project or not.

        DEMONSTRATION
        This is to be removed, used for demonstration purposes only. This category must not be included in your project.

        SAMPLE
        Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


        Detailed information and more samples can be found in the document.

        -->
        
        <script type="text/javascript">
            function check_delete()
            {
                var chk = confirm('Are You Sure To Delete This !');
                if(chk)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        </script>

    </head>

    <!--TIPS-->
    <!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

    <body>
        <div id="container" class="effect mainnav-lg">

            <!--NAVBAR-->
            <!--===================================================-->
            <header id="navbar">
                <div id="navbar-container" class="boxed">

                    <!--Brand logo & name-->
                    <!--================================-->
                    <div class="navbar-header">
                        <a href="dashboard.php" class="navbar-brand">
                            <img src="../asset/admin/img/logo.png" alt="Nifty Logo" class="brand-icon">
                            <div class="brand-title">
                                <span class="brand-text"><?php echo $_SESSION['admin_name']; ?></span>
                            </div>
                        </a>
                    </div>
                    <!--================================-->
                    <!--End brand logo & name-->


                    <!--Navbar Dropdown-->
                    <!--================================-->
                    <div class="navbar-content clearfix">
                        <ul class="nav navbar-top-links pull-left">

                            <!--Navigation toogle button-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <li class="tgl-menu-btn">
                                <a class="mainnav-toggle" href="#">
                                    <i class="fa fa-navicon fa-lg"></i>
                                </a>
                            </li>
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--End Navigation toogle button-->


                            <!--Messages Dropdown-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <li>
                                <a href="user_message.php"><i class="fa fa-envelope fa-lg"></i></a>
                            </li>
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--End message dropdown-->
                            
                            
                        </ul>
                        <ul class="nav navbar-top-links pull-right">

                            <!--User dropdown-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <li id="dropdown-user" class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                                    <span class="pull-right">
                                        <img class="img-circle img-user media-object" src="../asset/admin/img/av1.png" alt="Profile Picture">
                                    </span>
                                    <div class="username hidden-xs"><?php echo $_SESSION['admin_name']; ?></div>
                                </a>


                                <div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow panel-default">

                                    <!-- Dropdown heading  -->
                                    <div class="pad-all bord-btm">
                                        <p class="text-lg text-muted text-thin mar-btm">Welcome <b><?php echo $_SESSION['admin_name']; ?></b></p>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                    </div>


                                    <!-- User dropdown menu -->
                                    <ul class="head-list">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user fa-fw fa-lg"></i> Profile
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="label label-success pull-right">New</span>
                                                <i class="fa fa-gear fa-fw fa-lg"></i> Settings
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-question-circle fa-fw fa-lg"></i> Help
                                            </a>
                                        </li>
                                    </ul>

                                    <!-- Dropdown footer -->
                                    <div class="pad-all text-right">
                                        <a href="?l_id=logout" class="btn btn-primary">
                                            <i class="fa fa-sign-out fa-fw"></i> Logout
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--End user dropdown-->

                        </ul>
                    </div>
                    <!--================================-->
                    <!--End Navbar Dropdown-->

                </div>
            </header>
            <!--===================================================-->
            <!--END NAVBAR-->

            <div class="boxed">

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
                    <!--CONTENT CONTAINER-->
                    <!--===================================================-->
                    <div id="content-container">

                        <?php
                        if (isset($pages))
                        {
                            if($pages == 'add_category')
                            {
                                include ('pages/add_category_content.php');
                            }
                            elseif($pages == 'manage_category')
                            {
                            include ('pages/manage_category_content.php');
                            }
                            elseif($pages == 'edit_category')
                            {
                                include ('pages/edit_category_content.php');
                            }
                            elseif($pages == 'add_manufacturer')
                            {
                                include ('pages/add_manufacturer_content.php');
                            }
                            elseif($pages == 'manage_manufacturer')
                            {
                                include ('pages/manage_manufacturer_content.php');
                            }
                            elseif($pages == 'add_product')
                            {
                                include ('pages/add_product_content.php');
                            }
                            elseif($pages == 'manage_product')
                            {
                                include ('pages/manage_product_content.php');
                            }
                            elseif($pages == 'edit_manufacturer')
                            {
                                include ('pages/edit_manufacturer_content.php');
                            }
                            elseif($pages == 'edit_product')
                            {
                                include ('pages/edit_product_content.php');
                            }
                            elseif($pages == 'orderManage')
                            {
                                include ('pages/order_manage_content.php');
                            }
                            elseif($pages == 'viewOrder')
                            {
                                include ('./pages/view_order_product_content.php');;
                            }
                            elseif($pages == 'userMessage')
                            {
                                include ('./pages/user_message_content.php');
                            }
                            elseif($pages == 'userManage')
                            {
                                include ('./pages/msoul_user_manage_content.php');
                            }
                            elseif($pages == 'userStatus')
                            {
                                include ('./pages/users_status_content.php');
                            }
                            elseif($pages == 'userComment')
                            {
                                include_once ('./pages/user_comment_content.php');
                            }
                            elseif($pages == 'userAvatars')
                            {
                                include_once ('./pages/users_avatar_content.php');
                            }
                            elseif($pages == 'addSlider')
                            {
                                include_once ('./pages/add_slider_content.php');
                            }
                            elseif($pages == 'manageSlider')
                            {
                                include_once ('./pages/manage_slider_content.php');
                            }
                        }
                        else
                        {
                            include ('pages/dashboard_content.php');
                        }
                        ?>

                    </div>
                    <!--===================================================-->
                    <!--End page content-->


                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->



                <!--MAIN NAVIGATION-->
                <!--===================================================-->
                <nav id="mainnav-container">
                    <div id="mainnav">

                        <!--Shortcut buttons-->
                        <!--================================-->
                        <div id="mainnav-shortcut">
                            <ul class="list-unstyled">
                                <li class="col-xs-4" data-content="Additional Sidebar">
                                    <i class="fa fa-moon-o"></i>
                                </li>
                                <li class="col-xs-4" data-content="Notification">
                                    <i class="fa fa-moon-o"></i>
                                </li>
                                <li class="col-xs-4" data-content="Page Alerts">
                                    <i class="fa fa-moon-o"></i>
                                </li>
                            </ul>
                        </div>
                        <!--================================-->
                        <!--End shortcut buttons-->


                        <!--Menu-->
                        <!--================================-->
                        <div id="mainnav-menu-wrap">
                            <div class="nano">
                                <div class="nano-content">
                                    <ul id="mainnav-menu" class="list-group">

                                        <!--Category name-->
                                        <li class="list-header">Navigation</li>

                                        <!--Menu list item-->
                                        <li class="active-link">
                                            <a href="dashboard.php">
                                                <i class="fa fa-dashboard"></i>
                                                <span class="menu-title">
                                                    <strong>Dashboard</strong>
                                                    <span class="label label-success pull-right">Top</span>
                                                </span>
                                            </a>
                                        </li>

                                        <!--Menu list item-->
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-shopping-cart"></i>
                                                <span class="menu-title">
                                                    <strong>LittleShop</strong>
                                                </span>
                                                <i class="arrow"></i>
                                            </a>

                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="add_slider.php">Add Slider</a></li>
                                                <li><a href="manage_slider.php">Manage Slider</a></li>
                                                <li class="list-divider"></li>
                                                <li><a href="add_category.php">ADD Category</a></li>
                                                <li><a href="manage_category.php">Manage Category</a></li>
                                                <li class="list-divider"></li>
                                                <li><a href="add_manufacturer.php">ADD Manufacturer</a></li>
                                                <li><a href="manage_manufacturer.php">Manage Manufacturer</a></li>
                                                <li class="list-divider"></li>
                                                <li><a href="add_product.php">ADD Product</a></li>
                                                <li><a href="manage_product.php">Manage Product</a></li>
                                            </ul>
                                        </li>
                                        
                                        <!--Menu list item-->
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-shopping-cart"></i>
                                                <span class="menu-title">
                                                    <strong>Shipping Order</strong>
                                                </span>
                                                <i class="arrow"></i>
                                            </a>
                                            
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="order_manage.php">Order Manage</a></li>
                                            </ul>
                                        </li>
                                        <li class="list-divider"></li>
                                        
                                        <!--Mansoul User Mange-->
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user-plus"></i>
                                                <span class="menu-title">
                                                    <strong>ManSoul User Manage</strong>
                                                </span>
                                                <i class="arrow"></i>
                                            </a>
                                            
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="msoul_user_manage.php">Users Manage</a></li>
                                                <li><a href="users_avatar.php">Users Avatar Manage</a></li>
                                                <li><a href="users_status.php">Users Status Manage</a></li>
                                                <li><a href="user_comment.php">Users Comment Manage</a></li>
                                            </ul>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--================================-->
                        <!--End menu-->

                    </div>
                </nav>
            </div>



            <!-- FOOTER -->
            <!--===================================================-->
            <footer id="footer">

                <!-- Visible when footer positions are fixed -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="show-fixed pull-right">
                    <ul class="footer-list list-inline">
                        <li>
                            <p class="text-sm">SEO Proggres</p>
                            <div class="progress progress-sm progress-light-base">
                                <div style="width: 80%" class="progress-bar progress-bar-danger"></div>
                            </div>
                        </li>

                        <li>
                            <p class="text-sm">Online Tutorial</p>
                            <div class="progress progress-sm progress-light-base">
                                <div style="width: 80%" class="progress-bar progress-bar-primary"></div>
                            </div>
                        </li>
                        <li>
                            <button class="btn btn-sm btn-dark btn-active-success">Checkout</button>
                        </li>
                    </ul>
                </div>



                <!-- Visible when footer positions are static -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="hide-fixed pull-right pad-rgt">Currently v2.2</div>



                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

                <p class="pad-lft">&#0169; 2016 mSoul Social Network</p>



            </footer>
            <!--===================================================-->
            <!-- END FOOTER -->


            <!-- SCROLL TOP BUTTON -->
            <!--===================================================-->
            <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
            <!--===================================================-->

        </div>
        <!--===================================================-->
        <!-- END OF CONTAINER -->


        <!--JAVASCRIPT-->
        <!--=================================================-->

        <!--JSVAL [ VALIDATION ]-->
        <script type="text/javascript" src="../asset/admin/js/jsval.js"></script>
        
        
        <!--jQuery [ REQUIRED ]-->
        <script src="../asset/admin/js/jquery-2.1.1.min.js"></script>


        <!--BootstrapJS [ RECOMMENDED ]-->
        <script src="../asset/admin/js/bootstrap.min.js"></script>


        <!--Fast Click [ OPTIONAL ]-->
        <script src="../asset/admin/plugins/fast-click/fastclick.min.js"></script>


        <!--Nifty Admin [ RECOMMENDED ]-->
        <script src="../asset/admin/js/nifty.min.js"></script>


        <!--Morris.js [ OPTIONAL ]-->
        <script src="../asset/admin/plugins/morris-js/morris.min.js"></script>
        <script src="../asset/admin/plugins/morris-js/raphael-js/raphael.min.js"></script>


        <!--Sparkline [ OPTIONAL ]-->
        <script src="../asset/admin/plugins/sparkline/jquery.sparkline.min.js"></script>


        <!--Skycons [ OPTIONAL ]-->
        <script src="../asset/admin/plugins/skycons/skycons.min.js"></script>


        <!--Switchery [ OPTIONAL ]-->
        <script src="../asset/admin/plugins/switchery/switchery.min.js"></script>
        
        <!--DataTables [ OPTIONAL ]-->
        <script src="../asset/admin/plugins/datatables/media/js/jquery.dataTables.js"></script>
        <script src="../asset/admin/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
        <script src="../asset/admin/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
        
        <!--DataTables Sample [ SAMPLE ]-->
        <script src="../asset/admin/js/demo/tables-datatables.js"></script>

        <!--Bootstrap Select [ OPTIONAL ]-->
        <script src="../asset/admin/plugins/bootstrap-select/bootstrap-select.min.js"></script>
        
        <!--Chosen [ OPTIONAL ]-->
        <script src="../asset/admin/plugins/chosen/chosen.jquery.min.js"></script>
        
        <!--noUiSlider [ OPTIONAL ]-->
        <link href="../asset/admin/plugins/noUiSlider/jquery.nouislider.min.css" rel="stylesheet">
        <link href="../asset/admin/plugins/noUiSlider/jquery.nouislider.pips.min.css" rel="stylesheet">


        <!--Demo script [ DEMONSTRATION ]-->
        <script src="../asset/admin/js/demo/nifty-demo.min.js"></script>


        <!--Specify page [ SAMPLE ]-->
        <script src="../asset/admin/js/demo/dashboard.js"></script>


        <!--
    
        REQUIRED
        You must include this in your project.
    
        RECOMMENDED
        This category must be included but you may modify which plugins or components which should be included in your project.
    
        OPTIONAL
        Optional plugins. You may choose whether to include it in your project or not.
    
        DEMONSTRATION
        This is to be removed, used for demonstration purposes only. This category must not be included in your project.
    
        SAMPLE
        Some script samples which explain how to initialize plugins or components. This category should not be included in your project.
    
    
        Detailed information and more samples can be found in the document.
    
        -->


    </body>

    <!-- Mirrored from www.themeon.net/nifty/v2.2/dashboard.php by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:43:58 GMT -->
</html>
