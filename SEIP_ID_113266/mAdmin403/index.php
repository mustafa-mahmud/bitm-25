<?php
    require_once ('../classes/login.php');

    $obj_login = new Login();

    if(isset($_POST['btn']))
    {
        $message = $obj_login->admin_login_check($_POST);
    }

    if(isset($_SESSION['admin_id']))
    {
        header('Location: dashboard.php');
    }

?>

<!DOCTYPE html>
<html lang="en">


    <!-- Mirrored from www.themeon.net/nifty/v2.2/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:44:19 GMT -->
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login page | ManSoul</title>


        <!--STYLESHEET-->
        <!--=================================================-->

        <!--Open Sans Font [ OPTIONAL ] -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


        <!--Bootstrap Stylesheet [ REQUIRED ]-->
        <link href="../asset/admin/css/bootstrap.min.css" rel="stylesheet">


        <!--Nifty Stylesheet [ REQUIRED ]-->
        <link href="../asset/admin/css/nifty.min.css" rel="stylesheet">


        <!--Font Awesome [ OPTIONAL ]-->
        <link href="../asset/admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">


        <!--Demo [ DEMONSTRATION ]-->
        <link href="../asset/admin/css/demo/nifty-demo.min.css" rel="stylesheet">




        <!--SCRIPT-->
        <!--=================================================-->

        <!--Page Load Progress Bar [ OPTIONAL ]-->
        <link href="../asset/admin/plugins/pace/pace.min.css" rel="stylesheet">
        <script src="../asset/admin/plugins/pace/pace.min.js"></script>



        <!--

        REQUIRED
        You must include this in your project.

        RECOMMENDED
        This category must be included but you may modify which plugins or components which should be included in your project.

        OPTIONAL
        Optional plugins. You may choose whether to include it in your project or not.

        DEMONSTRATION
        This is to be removed, used for demonstration purposes only. This category must not be included in your project.

        SAMPLE
        Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


        Detailed information and more samples can be found in the document.

        -->


    </head>

    <!--TIPS-->
    <!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

    <body>
        <div id="container" class="cls-container">

            <!-- BACKGROUND IMAGE -->
            <!--===================================================-->
            <!--<div id="bg-overlay" class="bg-img img-balloon"></div>-->


            <!-- HEADER -->
            <!--===================================================-->
            <div class="cls-header cls-header-lg">
                <div class="cls-brand">
                    <a class="box-inline" href="index.php">
                        <!-- <img alt="Nifty Admin" src="../asset/admin/img/logo.png" class="brand-icon"> -->
                        <span class="brand-title">ManSoul <span class="text-thin">Admin Panel</span></span>
                    </a>
                </div>
            </div>
            <!--===================================================-->


            <!-- LOGIN FORM -->
            <!--===================================================-->
            <div class="cls-content">
                <div class="cls-content-sm panel">
                    <div class="panel-body">
                        <p class="pad-btm">Sign In to your account</p>
                        <form action="" method="post">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input type="email" class="form-control" placeholder="Email" name="admin_email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                    <input type="password" class="form-control" placeholder="Password" name="admin_password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-8 text-left checkbox">
                                    <label class="form-checkbox form-icon">
                                        <input type="checkbox"> Remember me
                                    </label>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group text-right">
                                        <button class="btn btn-success text-uppercase" type="submit" name="btn">Sign In</button>
                                    </div>
                                </div>
                                <div class="col-xs-8 text-center">
                                    <p class="text-danger"><?php if(isset($message)) { echo $message; unset($message); } ?></p>
                                    <p class="text-success"><?php if(isset($_SESSION['message'])){ echo $_SESSION['message']; unset($_SESSION['message']); } ?></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--===================================================-->

        </div>
        <!--===================================================-->
        <!-- END OF CONTAINER -->



        <!--JAVASCRIPT-->
        <!--=================================================-->

        <!--jQuery [ REQUIRED ]-->
        <script src="../asset/admin/js/jquery-2.1.1.min.js"></script>


        <!--BootstrapJS [ RECOMMENDED ]-->
        <script src="../asset/admin/js/bootstrap.min.js"></script>


        <!--Fast Click [ OPTIONAL ]-->
        <script src="../asset/admin/plugins/fast-click/fastclick.min.js"></script>


        <!--Nifty Admin [ RECOMMENDED ]-->
        <script src="../asset/admin/js/nifty.min.js"></script>


        <!--Background Image [ DEMONSTRATION ]-->
        <script src="../asset/admin/js/demo/bg-images.js"></script>


        <!--

        REQUIRED
        You must include this in your project.

        RECOMMENDED
        This category must be included but you may modify which plugins or components which should be included in your project.

        OPTIONAL
        Optional plugins. You may choose whether to include it in your project or not.

        DEMONSTRATION
        This is to be removed, used for demonstration purposes only. This category must not be included in your project.

        SAMPLE
        Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


        Detailed information and more samples can be found in the document.

        -->


    </body>

    <!-- Mirrored from www.themeon.net/nifty/v2.2/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Apr 2015 10:44:27 GMT -->
</html>
