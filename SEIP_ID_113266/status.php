<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Status <span style="font-size: 12px;"><?php display_message(); ?></span></h3>
    </div>
    <div class="panel-body">
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <textarea class="form-control" placeholder="Share on your mind" name="status_description" required="required"></textarea>
            </div>
            <button type="submit" name="status_bnt" class="btn btn-default pull-right">Post</button>
            <div class="pull-left">
                <div class="btn-group">
                    <input type="file" name="status_images" accept="image/gif, image/jpeg, image/png" class="btn btn-default">
                </div>
            </div>
        </form>
    </div>
</div><!-- end of Status -->