<div class="page-header">
    <h3>Selected Product <span class="pull-right"><a href="ordering_address.php" class="btn btn-success">ORDER</a></span></h3>
</div>
<table class="table table-bordered">
    <thead>
        <tr style="background: #000;">
            <th>Product Image</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $session_id = session_id();
        $result = $obj_cart->select_cart_product_by_session_id($session_id);
        $total = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            ?>
            <tr align="center">
                <td><img src="product_images/<?php echo $row['product_image']; ?>" alt="<?php echo $row['product_name'] ?>" style="width: 200px; height: 150px;" class="img-thumbnail"></td>
                <td><?php echo $row['product_name'] ?></td>
                <td>BDT <?php echo $row['product_price'] ?></td>
                <td>
                    <form action="" method="post">
                        <input type="text" value="<?php echo $row['product_sales_quantity'] ?>" name="sales_quantity" style="width: 50px; color: #000;">
                        <div class="clearfix"></div><br/>
                        <input type="hidden" value="<?php echo $row['proudct_id'] ?>" name="proudct_id">
                        <input type="submit" name="update_quantity" value="update" class="btn btn-success btn-sm">
                    </form>
                </td>
                <td>BDT <?php echo $subtotal = $row['product_price'] * $row['product_sales_quantity']; ?></td>
            </tr>
            <?php
            $total = $total + $subtotal;
        }
        ?>

        <tr>
            <td colspan="4"><p>Total</p></td>
            <td colspan="1">
                <p>BDT <?php echo $total; ?></p>
            </td>
        </tr>
        <tr>
            <td colspan="4"><p>Coupon Discount:</p></td>
            <td colspan="1">
                <p>BDT 
                    <?php
                    $discount = (($total * 10) / 100);
                    echo $discount;
                    ?>
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="4"><p>VAT Total:</p></td>
            <td colspan="1">
                <p>BDT 
                    <?php
                    $vat = (($total * 4.5) / 100);
                    echo $vat;
                    ?>
                </p>
            </td>
        </tr>
        <tr style="background-color: #000; font-weight: bold;">
            <td colspan="4"><p>Grand Total</p></td>
            <td colspan="1">
                <p>BDT 
                    <?php
                        $grand_total = $total+$vat - $discount;
                        $_SESSION['order_total'] = $grand_total;
                        echo $grand_total;
                    ?>
                </p>
            </td>
        </tr>
    </tbody>
</table>