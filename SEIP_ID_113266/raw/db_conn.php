<?php
$items_per_group = 2;

$db_conn = mysqli_connect('localhost', 'root', 'SADHIN101119', 'db_social_project') OR die(mysqli_error($db_conn));

function row_count($result)
{
    return mysqli_num_rows($result);
}

function escape($string)
{
    global $db_conn;
    return mysqli_real_escape_string($db_conn, $string);
}

function query($query)
{
    global $db_conn;
    $result = mysqli_query($db_conn, $query);
    confirm($result);
    return $result;
}

function getLastId()
{
    global $db_conn;
    return mysqli_insert_id($db_conn);
}

function confirm($result)
{
    global $db_conn;
    if(!$result)
    {
        die('QUERY FIELND ' .mysqli_error($db_conn). mysqli_connect_errno());
    }
}

function fetch_array($result)
{
    return mysqli_fetch_array($result);
}

function fetch_assoc($query_result)
{
    return mysqli_fetch_assoc($query_result);
}