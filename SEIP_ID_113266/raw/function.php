<?php

/********************* helper function ***********************/

function clean($string)
{
    return htmlentities($string);
}

function redirect($location)
{
    return header("Location: {$location}");
}

function set_message($message)
{
    if(!empty($message))
    {
        $_SESSION['message'] = $message;
    }
    else
    {
        $message = "";
    }
}

function display_message()
{
    if(isset($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
}

function token_generator()
{
    $token = $_SESSION['token'] = md5(uniqid(mt_rand(), TRUE));
    return $token;
}

function validation_errors($error_message)
{
    $error_message = <<<DELIMITER
            $error_message;
DELIMITER;
    return $error_message;
}

function email_exists($email)
{
    $info = "SELECT user_id FROM tbl_users WHERE email = '$email'";
    $result = query($info);
    
    if(row_count($result) == 1)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function send_email($email,$subject,$msg,$header)
{
    return mail($email, $subject, $msg,$header);
}

/********************* validation function ***********************/

function validate_user_registration()
{
    
    $errors = [];
    $min = 2;
    $max = 20;
    $emax = 32;
    
//    //if($_SERVER['REQUEST_METHOD'] == "POST")
    if(isset($_POST['btnReg']))
    {
        $first_name         = clean($_POST['first_name']);
        $last_name          = clean($_POST['last_name']);
        $email              = clean($_POST['email']);
        $date_of_birth      = clean($_POST['date_of_birth']);
        $gender             = clean($_POST['gender']);
        $password           = clean($_POST['password']);
        $confirm_password   = clean($_POST['confirm_password']);
        
        if(strlen($first_name) < $min )
        {
            $errors[] = "Your First Name can not be less then {$min} charecters";
        }
        if(strlen($first_name) > $max )
        {
            $errors[] = "Your First Name can not be more then {$max} charecters";
        }
        
        if(strlen($last_name) < $min )
        {
            $errors[] = "Your Last Name can not be less then {$min} charecters";
        }
        if(strlen($last_name) > $max )
        {
            $errors[] = "Your Last Name can not be more then {$max} charecters";
        }
        
        if(email_exists($email))
        {
            $errors[] = "Your email address allready exists";
        }
        
        if(strlen($email) > $emax )
        {
            $errors[] = "Your Email can not be more then {$max} charecters";
        }
        
        if($password !== $confirm_password )
        {
            $errors[] = "Your Password Is Not Match !";
        }
        
        if(!empty($errors))
        {
            foreach ($errors as $error)
            {
                echo validation_errors($error);
            }
        }
        else
        {
            if(rerister_user($first_name,$last_name,$email,$date_of_birth,$gender,$password))
            {
                set_message("Please check your email inbox or span folder for activation code");
                redirect("index.php");
            }
            else
            {
                set_message("Sorry, You are not registerted !");
                redirect("index.php");
            }
        }
    }
}

/********************* User Registration Function ***********************/

function rerister_user($first_name,$last_name,$email,$date_of_birth,$gender,$password)
{
    $first_name     = escape($first_name);
    $last_name      = escape($last_name);
    $email          = escape($email);
    $password       = escape($password);
    $date_of_birth  = escape($date_of_birth);
    $gender         = escape($gender);
    
    if(email_exists($email))
    {
        return FALSE;
    }
    else
    {
        $password = md5($password);
        $validation_code = md5($first_name + microtime());
        
        $info = "INSERT INTO tbl_users(first_name,last_name,email,password,date_of_birth,gender,validation_code, active)"
                . "VALUES('$first_name','$last_name','$email','$password','$date_of_birth','$gender','$validation_code',0)";
        
        $result = query($info);
        confirm($result);
        
        $subject    = "Man Soul - Activation Account";
        $msg        = "Please check the mail bwlow to active your account </br> http://localhost/project/activate.php?email=email&code=$validation_code";
        $header     = "From: norreply@jqurity.com";
        
        send_email($email, $subject, $msg, $header);
        
        return TRUE;
    }
}

/********************* Activate Usere Function ***********************/

function activate_user()
{
    if($_SERVER['REQUEST_METHOD'] == "GET")
    {
        if(isset($_GET['email']))
        {
            $email = clean($_GET['email']);
            $validation_code = clean($_GET['code']);
            
            $info = "SELECT user_id FROM tbl_users WHERE email = '". escape($_GET['email']) ."' AND validation_code = '". escape($_GET['code']) ."'";
            $result = query($info);
            confirm($result);
            if(row_count($result) == 1)
            {
                $info = "UPDATE tbl_users SET active = 1, validation_code = 0 WHERE email = '".escape($email)."' AND validation_code = '".  escape($validation_code)."' ";
                $q_result = query($info);
                confirm($q_result);
                set_message("Your account has been successfully activated. You can login now");
                redirect("index.php");
            }
            else
            {
                set_message("Sorry, Something is wrong ! Please registration again Or you are allready registered !");
                redirect("index.php");
            }
        }
    }
}

/********************* User Login Validation Function ***********************/

function validate_user_login()
{   
    $errors     = [];
    
    //if($_SERVER['REQUEST_METHOD'] == "POST")
    if(isset($_POST['btn']))
    {
        $email              = clean($_POST['email']);
        $password           = clean($_POST['password']);
        $remember           = isset($_POST['remember']);
        
        if(empty($email))
        {
            $errors[] = "Email fiend can not be empty";
        }
        
        if(empty($password))
        {
            $errors[] = "Password field can not be empty";
        }
        
        if(!empty($errors))
         {
            foreach ($errors as $error)
            {
                echo validation_errors($error);
            }
        }
        else
        {
            if(login_user($email, $password, $remember))
            {
                redirect("home.php");
            }
            else
            {
                echo validation_errors("Use valied username or password");
            }
        }
    }
}

/********************* User Login Function ***********************/

function login_user($email, $password, $remember)
{
    $info = "SELECT * FROM tbl_users WHERE email = '".escape($email)."' AND active = 1";
    $result = query($info);
    
    if(row_count($result) == 1)
    {
        $row = fetch_array($result);
        $db_password = $row['password'];
        if(md5($password) === $db_password )
        {
            if($remember == "on")
            {
                setcookie('email', $email, time() + 86400);
            }
            $_SESSION['user_id'] = $row['user_id'];
            $_SESSION['email'] = $email;
            $_SESSION['first_name'] = $row['first_name'];
            $_SESSION['last_name'] = $row['last_name'];
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/********************* User Logedin Function ***********************/

function loged_in()
{
    if(isset($_SESSION['email']) || isset($_COOKIE['email']))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/****************** User Password Recovered Function ********************/

function recovered_password()
{
    //if(isset($_POST['R_btn']))
    if($_SERVER['REQUEST_METHOD'] == "POST")
    {
        $email = clean($_POST['email']);
        if(email_exists($email))
        {
        $validation_code = md5($email + microtime());
                
        setcookie('temp_access_code', $validation_code, time() + 900);
                
        $info = "UPDATE tbl_users SET validation_code = '".escape($validation_code)."' WHERE email = '".escape($email)."' ";
        $result = query($info);
        confirm($result);
                
        $subject = "Please reset your password";
        $msg = "You activation code is under the blow </br> {$validation_code} </br> Click here to reset your password: http://localhost/project/a_code.php?email=$send&code=$validation_code";
        $header = "From: noreply@jqurity.com";
                
            if(!send_email($email, $subject, $msg, $header))
        {
            echo validation_errors("Email cound not be sent !");
        }
            set_message("Please chekc your email or spam folder for password reset");
             redirect("index.php");
        }
        else
        {
            echo validation_errors("This email does not exist !");
        }
    }
}

/********************* Code Validation ***********************/

function validaiton_code()
{
    if(isset($_COOKIE['temp_access_code']))
    {
            if(!isset($_GET['email']) && !isset($_GET['code']))
            {
                //redirect("index.php");
            }
            elseif(empty ($_GET['email']) || empty ($_GET['code']))
            {
                redirect("index.php");
            }
            else
            {
                if(isset($_POST['code']))
                {
                    $email = clean($_GET['email']);
                    $validation_code = clean(($_POST['code']));
                    $info = "SELECT user_id FROM tbl_users WHERE validation_code = '".escape($validation_code)."' AND email = '".escape($email)."'";
                    $result = query($info);
                    confirm($result);
                    
                    if(row_count($result) == 1)
                    {
                        setcookie('temp_access_code', $validation_code, time() + 900);
                        redirect("reset.php?email=$email&code=$validation_code");
                    }
                    else
                    {
                        echo validaiton_code("Sorry wrong validation code");
                    }
                }
            }
    }
    else
    {
        set_message("Sorry your validation time was expire !");
//        redirect("recover_password.php");
    }
}

/********************* Password Reset Function ***********************/

function password_reset()
{
    if(isset($_COOKIE['temp_access_code']))
    {
        if(isset($_GET['email']) && isset($_GET['code']))
        {
            if(isset($_SESSION['token']) && isset($_POST['token']))
            {
                if($_POST['password'] == $_POST['confirm_password'])
                {
                    $updated_password = md5($_POST['password']);
                    $info = "UPDATE tbl_users SET password = '".escape($updated_password)."', validation_code = 0 WHERE email = '".  escape($_GET['email'])."' ";
                    $result = query($info);
                    confirm($result);
                    set_message("Your password successfully updated. You can login Now");
                    redirect("index.php");
                }
            }
        }
    }
    else
    {
        set_message("Sorry, Your session time expired !");
        redirect("recover_password.php");
    }
}

/********************* Save User Info Function ***********************/

function save_user_info($data)
{
    $profession             = escape($data['profession']);
    $school_name            = escape($data['school_name']);
    $college_name           = escape($data['college_name']);
    $university_name        = escape($data['university_name']);
    $currect_zone           = escape($data['currect_zone']);
    $home_zone              = escape($data['home_zone']);
    $relationship_status    = escape($data['relationship_status']);
    $username               = escape($data['username']);
    $religious              = escape($data['religious']);
    $about_me               = escape($data['about_me']);
    $user_id                = $_SESSION['user_id'];
    
    $info = "UPDATE tbl_users SET profession = '$profession', school_name = '$school_name', college_name = '$college_name', university_name = '$university_name', currect_zone = '$currect_zone',"
            . "home_zone = '$home_zone', relationship_status = '$relationship_status', username = '$username', religious = '$religious', about_me = '$about_me' WHERE user_id = $user_id";
    query($info);
    set_message("<span class='text-success'>Your Information is successfully Update<span>");
    redirect('about.php');
}

/********************* Upload User Avatar Function ***********************/

function upload_avatar($data,$files)
{
    $user_id        = $_SESSION['user_id'];
    
    if ($files['users_avator']['name'])
    {
        $target_dir = "users_avatar/";
        $target_file = $target_dir . basename($files["users_avator"]["name"]);
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        $check = getimagesize($files["users_avator"]["tmp_name"]);

        if ($check !== FALSE)
        {
            if ($files['users_avator']['size'] < 2000000)
            {
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg")
                {
                    set_message("Sorry, only JPG, JPEG and PNG files are allowed");
                }
                else
                {
                    if (move_uploaded_file($files['users_avator']['tmp_name'], $target_file))
                    {
                        $users_avator = $target_file;
                        $info = "UPDATE tbl_users SET users_avator = '$users_avator' WHERE user_id = '$user_id'";
                        query($info);
                    }
                    else
                    {
                        set_message ("Sorry, there was an error uploading your file");
                    }
                }
            }
            else
            {
                set_message("Sorry, Your image file is too large");
            }
        }
        else
        {
            set_message("File is not an image");
        }
    }
}

/********************* Upload Cover Photo Function ***********************/

function upload_cover_photo($data, $files)
{
    $user_id = $_SESSION['user_id'];
    
    if ($files['cover_photo']['name'])
    {
        $target_dir = "cover_photos/";
        $target_file = $target_dir . basename($files["cover_photo"]["name"]);
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        $check = getimagesize($files["cover_photo"]["tmp_name"]);

        if ($check !== FALSE)
        {
            if ($files['cover_photo']['size'] < 3000000)
            {
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg")
                {
                    set_message("Sorry, only JPG, JPEG and PNG files are allowed");
                }
                else
                {
                    if (move_uploaded_file($files['cover_photo']['tmp_name'], $target_file))
                    {
                        $cover_photo = $target_file;
                        $info = "UPDATE tbl_users SET cover_photo = '$cover_photo' WHERE user_id = '$user_id'";
                        query($info);
                    }
                    else
                    {
                        set_message ("Sorry, there was an error uploading your file");
                    }
                }
            }
            else
            {
                set_message("Sorry, Your image file is too large");
            }
        }
        else
        {
            set_message("File is not an image");
        }
    }
}

/********************* View all Info of User Function ***********************/

function select_all_user_info()
{
    $user_id = $_SESSION['user_id'];
    
    $info = "SELECT * FROM tbl_users WHERE user_id = '$user_id'";
    $query_result = query($info);
    $result = fetch_assoc($query_result);
    return $result;
}

/********************* View Users Info Function ***********************/

function select_all_users_info()
{
    $info = "SELECT * FROM tbl_users WHERE active = 1";
    $query_result = query($info);
    return $query_result;
}

/********************* View Users Info LIMIT Function ***********************/

function select_all_user_info_limit()
{
    $info = "SELECT * FROM tbl_users WHERE active = 1 LIMIT 12";
    $query_result = query($info);
    return $query_result;
}

/********************* Select Users Info By ID ***********************/

function select_all_user_by_user_id($user_id)
{
    $info = "SELECT * FROM tbl_users WHERE user_id = $user_id";
    $result = query($info);
    $query_result = fetch_assoc($result);
    return $query_result;
}

/********************* View Users Info By ID ***********************/

function select_all_user_info_by_user_id($user_id)
{
    $info = "SELECT * FROM tbl_users WHERE user_id = $user_id";
    $result = query($info);
    $query_result = fetch_assoc($result);
    return $query_result;
}

/********************* Insert Pending List Function ***********************/

/********************* Select Pending List Function ***********************/

/********************* Give Status Upload Function ***********************/

function give_status($data,$files)
{
    $status_description = escape($data['status_description']);
    $user_id            = $_SESSION["user_id"];
    $info = "INSERT INTO tbl_status (status_description,user_id) VALUES ('$status_description','$user_id')";
    query($info);
    $last_id = getLastId();
    
    if ($files['status_images']['name'])
    {
        $target_dir = "status_images/";
        $target_file = $target_dir . basename($files["status_images"]["name"]);
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        $check = getimagesize($files["status_images"]["tmp_name"]);

        if ($check !== FALSE)
        {
            if ($files['status_images']['size'] < 2000000)
            {
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg")
                {
                    set_message("Sorry, only JPG, JPEG and PNG files are allowed");
                }
                else
                {
                    if (move_uploaded_file($files['status_images']['tmp_name'], $target_file))
                    {
                        $status_images = $target_file;
                        $info = "UPDATE tbl_status SET status_images='$status_images' WHERE status_id='$last_id ' LIMIT 1";
                        query($info);
                    }
                }
            }
            else
            {
                set_message("Sorry, Your image file is too large");
            }
        }
        else
        {
            set_message("File is not an image");
        }
    }
}

/********************* Status Result Function ***********************/

function select_all_status_by_user_id()
{
    $user_id = $_SESSION['user_id'];
    
    $info = "SELECT * FROM tbl_status WHERE user_id = '$user_id' ORDER BY status_id DESC";
    $query_result = query($info);
    return $query_result;
}

/********************* Comment Function ***********************/

function give_comment($data)
{
    $comment_description    = escape($data['comment_description']);
    $status_id              = escape($data['id']);
    $user_id                = $_SESSION['user_id'];
    
    $info = "INSERT INTO tbl_comment (comment_description,status_id,user_id) VALUES ('$comment_description','$status_id','$user_id')";
    query($info);
}

/********************* Comment Result Function ***********************/

function select_all_comment_by_comment_id($status_id)
{
    $user_id    = $_SESSION['user_id'];
    
    $info = "SELECT * FROM tbl_comment WHERE status_id = '$status_id' ORDER BY comment_id DESC";
    $query_result = query($info);
    return $query_result;
}

/********************* Select All Users Status Function ***********************/

function select_all_users_status()
{
    $info = "SELECT * FROM tbl_status ORDER BY status_id DESC";
    $query_result = query($info);
    return $query_result;
}

/********************* Select Single User Function ***********************/

function select_user_by_id($user_id)
{
    $info = "SELECT * FROM tbl_users WHERE user_id = '$user_id' LIMIT 1 ";
    $result = query($info);
    return $result;
}

/********************* Select Image Gallery ***********************/

function select_all_status_image($user_id)
{
    $info = "SELECT * FROM tbl_status WHERE user_id = '$user_id'";
    $query_result = query($info);
    return $query_result;
}

/********************* Like Function ***********************/

function increase_likes($status_id)
{
    $info = "UPDATE tbl_status SET status_likes = (status_likes + 1) WHERE status_id = '$status_id' LIMIT 1";
    query($info);
}

/********************* User Logout Function ***********************/

function user_logout()
{
    if(loged_in())
    {
        session_destroy();
        redirect("index.php");
    }
}