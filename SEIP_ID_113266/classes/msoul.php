<?php
require_once ('db_conn.php');

/**
 * Description of Msoul
 *
 * @author Mohin Jaki
 */
class Msoul {
    private $obj_db;
    public function __construct()
    {
        $this->obj_db = new DB_conn();
    }
    
    public function select_all_msoul_user_info()
    {
        $info = "SELECT * FROM tbl_users";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
    
    public function select_users_status()
    {
        $info = "SELECT u.user_id, u.first_name, u.last_name, u.username, s.* FROM tbl_users as u, tbl_status as s";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
    
    public function select_users_comment()
    {
        $info = "SELECT u.user_id, u.first_name, u.last_name, u.username, c.* FROM tbl_users as u, tbl_comment as c";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
    
    public function user_activity_uncheck($user_id)
    {
        $info = "UPDATE tbl_users SET active = '0' WHERE user_id = '$user_id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: msoul_user_manage.php');
    }
    public function user_activity_check($user_id)
    {
        $info = "UPDATE tbl_users SET active = '1' WHERE user_id = '$user_id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: msoul_user_manage.php');
    }
    
    public function delete_user($user_id)
    {
        $info = "DELETE FROM tbl_users WHERE user_id = '$user_id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: msoul_user_manage.php');
    }
    
    public function delete_status_by_status_id($status_id)
    {
        $info = "DELETE FROM tbl_status WHERE status_id = '$status_id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: users_status.php');
    }
    public function delete_comment_by_comment($comment_id)
    {
        $info = "DELETE FROM tbl_comment WHERE comment_id = '$comment_id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: user_comment.php');
    }
}
