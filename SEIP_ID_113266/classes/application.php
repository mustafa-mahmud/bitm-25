<?php
require_once ('db_conn.php');

/**
 * Description of application
 *
 * @author Mohin Jaki
 */

class Application {
    private $obj_db;
    public function __construct()
    {
        $this->obj_db = new DB_conn();
    }
    
    public function select_published_category()
    {
        $info = "SELECT * FROM tbl_category WHERE publication_status='1'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
    
    public function select_all_published_product()
    {
        $info = "SELECT * FROM tbl_product WHERE publication_status='1' LIMIT 6";
        $query_resut = mysqli_query($this->obj_db->get_db(), $info);
        return $query_resut;
    }
    
    public function select_all_published_category_by_id($category_id)
    {
        $info = "SELECT * FROM tbl_product WHERE publication_status = '1' AND category_id='$category_id'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }

    public function select_product_by_product_id($product_id)
    {
        $info = "SELECT * FROM tbl_product WHERE product_id = '$product_id'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
}