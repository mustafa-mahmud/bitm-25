<?php
require_once ('db_conn.php');
/**
 * Description of Cart
 *
 * @author Mohin Jaki
 */
class Cart {
    private $obj_db;
    public function __construct()
    {
        $this->obj_db = new DB_conn();
    }
    
    public function add_to_cart($data)
    {
        $product_id = $data['product_id'];
        $info = "SELECT * FROM tbl_product WHERE product_id = '$product_id'";
        $result = mysqli_query($this->obj_db->get_db(), $info);
        $product_info = mysqli_fetch_assoc($result);
        
        $session_id = session_id();
        $product_sales_quantity = $data['sales_quantity'];
        
        $infoCheck = "SELECT * FROM tbl_cart_temp WHERE session_id = '$session_id' AND product_id = '$product_id'";
        $resultCheck = mysqli_query($this->obj_db->get_db(), $infoCheck);
        $product_info_check = mysqli_fetch_assoc($resultCheck);
        if($product_info_check)
        {
            header('Location: cart.php');
        }
        
        $info = "INSERT INTO tbl_cart_temp (session_id,product_image,proudct_id,product_name,product_price,product_sales_quantity)"
                . "VALUES ('$session_id','$product_info[product_image]','$data[product_id]','$product_info[product_name]','$product_info[product_price]','$product_sales_quantity')";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: cart.php');
    }
    
    public function select_cart_product_by_session_id($session_id)
    {
        $info = "SELECT * FROM tbl_cart_temp WHERE session_id = '$session_id'";
        $result = mysqli_query($this->obj_db->get_db(), $info);
        return $result;
    }
    
    public function update_product_sales_quantity($data)
    {
        $proudct_id = $data['proudct_id'];
        $product_sales_quantity = $data['sales_quantity'];
        $info = "UPDATE tbl_cart_temp SET product_sales_quantity ='$product_sales_quantity' WHERE proudct_id = '$proudct_id'";
        mysqli_query($this->obj_db->get_db(), $info);
    }
    
    public function remove_cart_product($cart_id)
    {
        $info = "DELETE FROM tbl_cart_temp WHERE cart_id = '$cart_id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: cart.php');
    }
    
    /**
     * ADMIN SECTION START HERE
     */
    
    public function select_all_order_info()
    {
        $info = "SELECT o.*, u.first_name, u.last_name, p.payment_status FROM tbl_order as o, tbl_users as u, tbl_payment as p WHERE o.user_id = u.user_id AND o.payment_id = p.payment_id";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
    
    public function select_customer_info_by_order_id($order_id)
    {
        $info = "SELECT o.order_id, o.user_id, u.*, s.* FROM tbl_order as o, tbl_users as u, tbl_shipping as s WHERE o.user_id = u.user_id AND o.order_id = '$order_id'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
    
    public function select_product_info_by_order_id($order_id)
    {
        $info = "SELECT o.order_id, o.order_total, od.* FROM tbl_order as o, tbl_order_details as od WHERE o.order_id = od.order_id AND o.order_id = '$order_id'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
    
}
