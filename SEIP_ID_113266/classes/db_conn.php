<?php

/**
 * Description of DB_conn
 * this is mansoul or msoul social network website database connection file
 * @author Mohin Jaki
 */
class DB_conn {
    private $db_conn;
    public function __construct() {
        $hostname = 'localhost';
        $username = 'root';
        $password = 'SADHIN101119';
        $db_name = 'db_social_project';
        
        $this->db_conn = mysqli_connect($hostname, $username, $password);
        if($this->db_conn)
        {
//            echo 'database connect successfully';
            mysqli_select_db($this->db_conn, $db_name);
        }
        else
        {
            die('CONNECTION ERROR !'.mysqli_connect_error($this->db_conn));
        }
    }
    
    public function get_db()
    {
        return $this->db_conn;
    }
}
