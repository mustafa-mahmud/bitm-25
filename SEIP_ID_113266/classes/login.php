<?php
require_once ('db_conn.php');
/**
 * Description of Login
 *
 * @author Mohin Jaki
 */
class Login {
    public function __construct() {
        $this->obj_db = new DB_conn();
        session_start();
    }
    
    public function admin_login_check($data)
    {
        $password = md5($data['admin_password']);
        $info = "SELECT * FROM tbl_admin WHERE admin_email='$data[admin_email]' AND admin_password='$password'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        $result = mysqli_fetch_assoc($query_result);
//        echo '<pre>';
//        print_r($result);
        if($result)
        {
            $_SESSION['admin_id']=$result['admin_id'];
            $_SESSION['admin_name']=$result['admin_name'];
            $this->get_session(TRUE);
            header('Location: dashboard.php');
        }
        else
        {
            $message = "Your Email Or Password Invalied !";
            return $message;
        }
    }
    
    public function get_session($gstatus = NULL)
    {
        if($_SESSION['session_status'] == $gstatus)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
