<?php

require_once ('db_conn.php');

/**
 * Description of Admin
 *
 * @author Mohin Jaki
 */
class Admin {
    private $obj_db;
    public function __construct() {
        $this->obj_db = new DB_conn();
    }

    /***************************************************************************
     * Category Section
    ***************************************************************************/

    /**
     * insert data tbl_category
     * @param type $data
     * @return string message
     */
    public function save_category_info($data) {
        $info = "INSERT INTO tbl_category (category_name,category_description,publication_status) VALUES('$data[category_name]','$data[category_description]','$data[publication_status]')";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        if ($query_result) {
            $message = 'Category info save successfully';
            return $message;
        } else {
            die(mysqli_connect_error());
        }
    }

    /**
     * view all data of tbl_category
     * @return type data
     */
    public function select_all_category() {
        $info = "SELECT * FROM tbl_category WHERE deletion_status = 0";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }

    /**
     * published - unpublished and delete tbl_category 
     * @param type $id
     */
    public function published_category($id) {
        $info = "UPDATE tbl_category SET publication_status = '1' WHERE category_id = '$id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_category.php');
    }

    public function unpublished_category($id) {
        $info = "UPDATE tbl_category SET publication_status = '2' WHERE category_id = '$id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_category.php');
    }

    public function delete_category($id) {
//        $info = "DELETE FROM tbl_category WHERE category_id = '$id'";
        $info = "UPDATE tbl_category SET deletion_status = 1 WHERE category_id = '$id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_category.php');
    }

    /**
     * update tbl_category data
     * @param type $category_id
     * @return type data
     */
    public function select_category_info_by_id($category_id) {
        $info = "SELECT * FROM tbl_category WHERE category_id = '$category_id'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }

    public function update_category_info($data) {
        $info = "UPDATE tbl_category SET category_name='$data[category_name]',category_description='$data[category_description]',publication_status='$data[publication_status]' WHERE category_id='$data[category_id]'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_category.php');
    }
    
    public function category_name_ajax_search($category_name)
    {
        $info = "SELECT * FROM tbl_category WHERE category_name = '$category_name'";
        $result = mysqli_query($this->obj_db->get_db(), $info);
        return $result;
    }

    /***************************************************************************
     * Manufacturer Section
    ************************************************************************** */

    /**
     * insert data tbl_manufacturer
     * @param type $data
     */
    public function save_manufacturer_info($data, $files)
    {
        {
            if ($files['manufacturer_image']['name'])
            {
                $target_dir = "../manufacturer_images/";
                $target_file = $target_dir . basename($files["manufacturer_image"]["name"]);
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                $check = getimagesize($files["manufacturer_image"]["tmp_name"]);

                if ($check !== FALSE) {
                    if ($files['manufacturer_image']['size'] < 2000000) {
                        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                            $message = 'Sorry, only JPG, JPEG, PNG & GIF files are allowed';
                            return $message;
                        } else {
                            if (move_uploaded_file($files['manufacturer_image']['tmp_name'], $target_file)) {
                                $manufacturer_image = $target_file;
                                $info = "INSERT INTO tbl_manufacturer(manufacturer_name,manufacturer_description,manufacturer_image,publication_status) VALUES('$data[manufacturer_name]','$data[manufacturer_description]','$manufacturer_image','$data[publication_status]')";
                                if (mysqli_query($this->obj_db->get_db(), $info)) {
                                    $message = 'Manufacturer info save successfully';
                                    return $message;
                                } else {
                                    die('ERROR UPLOADING ' . mysqli_error($this->obj_db->get_db()));
                                }
                            } else {
                                $message = 'Sorry, there was an error uploading your file';
                                return $message;
                            }
                        }
                    } else {
                        $message = 'Sorry, Your image file is too large';
                        return $message;
                    }
                } else {
                    $message = 'File is not an image';
                    return $message;
                }
            }
            else
            {
                $message = 'Image file not selected';
                return $message;
            }
        }
    }

    /**
     * view data tbl_manufacturer
     * @return type data
     */
    public function select_all_manufacturer() {
        $info = "SELECT * FROM tbl_manufacturer WHERE deletion_status = 0";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }

    /**
     * published - unpublished - delete from tbl_manufacturer
     * @param type $id
     */
    public function unpublished_manufacturer($id) {
        $info = "UPDATE tbl_manufacturer SET publication_status = '2' WHERE manufacturer_id = '$id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_manufacturer.php');
    }

    public function published_manufacturer($id) {
        $info = "UPDATE tbl_manufacturer SET publication_status = '1' WHERE manufacturer_id = '$id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_manufacturer.php');
    }

    public function delete_manufacturer($id) {
        $info = "UPDATE tbl_manufacturer SET deletion_status = '1' WHERE manufacturer_id = '$id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_manufacturer.php');
    }
    
    /**
     * update data tbl_manufacturer
     * @param type $manufacturer_id
     * @return type data
     */
    
    public function selece_manufacturer_info_by_id($manufacturer_id)
    {
        $info = "SELECT * FROM tbl_manufacturer WHERE manufacturer_id = '$manufacturer_id'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }

    public function update_manufacturer_info($data)
    {
        $info = "UPDATE tbl_manufacturer SET manufacturer_name='$data[manufacturer_name]',manufacturer_description='$data[manufacturer_description]',publication_status='$data[publication_status]' WHERE manufacturer_id = '$data[manufacturer_id]'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_manufacturer.php');
    }

    /***************************************************************************
     * Product Section
    ***************************************************************************/

    public function select_published_category()
    {
        $info = "SELECT * FROM tbl_category WHERE publication_status = '1'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
    
    public function select_published_manufacturer()
    {
        $info = "SELECT * FROM tbl_manufacturer WHERE publication_status = '1'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }

    /**
     * insert data from tbl_product
     * @param type $data
     * @param type $files
     */
    public function save_product_info($data, $files)
    {
        {
            if ($files['product_image']['name']) {
                $target_dir = "../product_images/";
                $target_file = $target_dir . basename($files["product_image"]["name"]);
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                $check = getimagesize($files["product_image"]["tmp_name"]);

                if ($check !== FALSE) {
                    if ($files['product_image']['size'] < 2000000) {
                        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                            $message = 'Sorry, only JPG, JPEG, PNG & GIF files are allowed';
                            return $message;
                        } else {
                            if (move_uploaded_file($files['product_image']['tmp_name'], $target_file)) {
                                $product_image  = $target_file;
                                $product_name  = $data['product_name'];
                                $category_id    = $data['category_id'];
                                $manufacturer_id= $data['manufacturer_id'];
                                $product_price  = $data['product_price'];
                                $product_quantity = $data['product_quantity'];
                                $product_sku    = $data['product_sku'];
                                $product_short_description = $data['product_short_description'];
                                $product_long_description = $data['product_long_description'];
                                $publication_status = $data['publication_status'];
                                $info = "INSERT INTO tbl_product (product_name,category_id,manufacturer_id,product_price,product_quantity,product_sku,product_short_description,product_long_description,product_image,publication_status) VALUES"
                                        . "('$product_name','$category_id','$manufacturer_id','$product_price','$product_quantity','$product_sku','$product_short_description','$product_long_description','$product_image','$publication_status')";
//                                $info = "INSERT INTO tbl_product (product_name,category_id,manufacturer_id,product_price,product_quantity,product_sku,product_short_description,product_long_description,product_image,publication_status) VALUES"
//                                        . " ('$data[product_name]','$data[category_id]','$data[manufacturer_id]','$data[product_price]','$data[product_quantity]','$data[product_sku]','$data[product_short_description]','$data[product_long_description]','$product_image','$data[publication_status]')";
//                                $info = "INSERT INTO tbl_product (product_name,category_id,manufacturer_id,product_price,product_quantity,product_sku,product_short_description,product_long_description,product_image,publication_status) VALUES('$data[product_name]','$data[category_id]',"
//                                    . "'$data[manufacturer_id]','$data[product_price]','$data[product_quantity]','$data[product_sku]',"
//                                    . "'$data[product_short_description]','$data[product_long_description]','$product_image','$data[publication_status]')";
                                if (mysqli_query($this->obj_db->get_db(), $info))
                                {
                                    $message = 'Manufacturer info save successfully';
                                    return $message;
                                } else {
                                    die('ERROR UPLOADING ' . mysqli_error($this->obj_db->get_db()));
                                }
                            } else {
                                $message = 'Sorry, there was an error uploading your file';
                                return $message;
                            }
                        }
                    } else {
                        $message = 'Sorry, Your image file is too large';
                        return $message;
                    }
                } else {
                    $message = 'File is not an image';
                    return $message;
                }
            } else {
                $message = 'Image file not selected';
                return $message;
            }
        }
    }
    
    /**
     * vew all data tbl_product
     * @return type data
     */
    
    public function select_all_product_info()
    {
        $info = "SELECT * FROM tbl_product WHERE deletion_status = '0'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }
    
    /**
     * published - unpublished - delete data from tbl_product
     * @param type $id
     */
    
    public function published_product($id)
    {
        $info = "UPDATE tbl_product SET publication_status = '2' WHERE product_id = '$id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_product.php');
    }
    
    public function uppublished_product($id)
    {
        $info = "UPDATE tbl_product SET publication_status = '1' WHERE product_id = '$id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_product.php');
    }
    
    public function delete_product($id)
    {
        $info = "UPDATE tbl_product SET deletion_status = '1' WHERE product_id = '$id'";
        mysqli_query($this->obj_db->get_db(), $info);
        header('Location: manage_product.php');
    }
    
    /**
     * update tbl_product
     * @param type $product_id
     * @param type $data
     * @return type data
     */
    
    public function select_product_info_by_id($product_id)
    {
        $info = "SELECT * FROM tbl_product WHERE publication_status = '1'";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
        
    }
    
    public function update_product_info($data)
    {
        $info = "UPDATE tbl_product SET product_name='$data[product_name]',category_id='$data[category_id]',manufacturer_id='$data[manufacturer_id]',"
                . "product_price='$data[product_price]',product_quantity='$data[product_quantity]',product_sku='$data[product_sku]',"
                . "product_short_description='$data[product_short_description]',product_long_description='$data[product_long_description]',"
                . "publication_status='$data[publication_status]' WHERE product_id = '$data[product_id]'";
        $row = mysqli_query($this->obj_db->get_db(), $info);
        if($row){
        header('Location: manage_product.php');
        } else{
            die(mysqli_connect_error());
        }
    }
    
    /**
     * SLIDER ADDING
     */
    
    public function add_slider($data,$files)
    {
        {
            if ($files['slider_image']['name'])
            {
                $target_dir = "../slider/";
                $target_file = $target_dir . basename($files["slider_image"]["name"]);
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                $check = getimagesize($files["slider_image"]["tmp_name"]);

                if ($check !== FALSE) {
                    if ($files['slider_image']['size'] < 2000000) {
                        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                            $message = 'Sorry, only JPG, JPEG, PNG & GIF files are allowed';
                            return $message;
                        } else {
                            if (move_uploaded_file($files['slider_image']['tmp_name'], $target_file)) {
                                $slider_image = $target_file;
                                $info = "INSERT INTO tbl_slider (slider_name,slider_image,publication_status) VALUES ('$data[slider_name]','$slider_image','$data[publication_status]')";
                                if (mysqli_query($this->obj_db->get_db(), $info)) {
                                    $message = 'Slider info save successfully';
                                    return $message;
                                } else {
                                    die('ERROR UPLOADING ' . mysqli_error($this->obj_db->get_db()));
                                }
                            } else {
                                $message = 'Sorry, there was an error uploading your file';
                                return $message;
                            }
                        }
                    } else {
                        $message = 'Sorry, Your image file is too large';
                        return $message;
                    }
                } else {
                    $message = 'File is not an image';
                    return $message;
                }
            }
            else
            {
                $message = 'Image file not selected';
                return $message;
            }
        }
    }
    
    /**
     * View All Slider
     */
    
    public function select_all_slider()
    {
        $info = "SELECT * FROM tbl_slider WHERE deletion_status = 0";
        $query_result = mysqli_query($this->obj_db->get_db(), $info);
        return $query_result;
    }

    /**
     * logout
    */
    public function logout() {
        session_destroy();
        session_start();
        $_SESSION['message'] = 'You Are Successfully Logout !';
        header('Location: index.php');
    }

}
