<?php
require_once ('db_conn.php');

/**
 * Description of Shipping
 *
 * @author Mohin Jaki
 */
class Shipping {
    private $obj_db;
    public function __construct() {
        $this->obj_db = new DB_conn();
    }
    
    public function save_product_shipping_info($data)
    {
        $info = "INSERT INTO tbl_shipping (full_name,email_address,address,phone_number,city,country,zip_code) VALUES ('$data[full_name]','$data[email_address]','$data[address]','$data[phone_number]','$data[city]','$data[country]','$data[zip_code]')";
        mysqli_query($this->obj_db->get_db(), $info);
        $shipping_id = mysqli_insert_id($this->obj_db->get_db());
        $_SESSION['shipping_id']=$shipping_id;
        header('Location: payment.php');
    }
    
    public function save_order_info($data)
    {
        $payment_type = $data['payment'];
        if($payment_type == 'case_on_delivery')
        {
            $info = "INSERT INTO tbl_payment (payment_type) VALUES ('$payment_type')";
            mysqli_query($this->obj_db->get_db(), $info);
            $payment_id = mysqli_insert_id($this->obj_db->get_db());
            $_SESSION['payment_id'] = $payment_id;
            $info_order = "INSERT INTO tbl_order(user_id,shipping_id,payment_id,order_total) VALUES ('$_SESSION[user_id]','$_SESSION[shipping_id]','$payment_id','$_SESSION[order_total]')";
            mysqli_query($this->obj_db->get_db(), $info_order);
            $order_id = mysqli_insert_id($this->obj_db->get_db());
            $_SESSION['order_id'] = $order_id;
            $session_id = session_id();
            $info_all_product = "SELECT * FROM tbl_cart_temp WHERE session_id = '$session_id'";
            $query_result = mysqli_query($this->obj_db->get_db(), $info_all_product);
                while ($cart_product = mysqli_fetch_assoc($query_result))
                {
                    $info_order_details = "INSERT INTO tbl_order_details(order_id,proudct_id,product_name,product_price,product_sales_quantity,product_image) VALUES"
                            . "('$order_id','$cart_product[proudct_id]','$cart_product[product_name]','$cart_product[product_price]','$cart_product[product_sales_quantity]','$cart_product[product_image]')";
                    mysqli_query($this->obj_db->get_db(), $info_order_details);
                }
                $info_clear_tbl_temp = "DELETE FROM tbl_cart_temp WHERE session_id = '$session_id'";
                mysqli_query($this->obj_db->get_db(), $info_clear_tbl_temp);
                header('Location: home.php');
        }
        elseif($payment_type == 'paypal')
        {
            echo 'Process Not Completed ! Please Selected Case On Delivery :) <br/> Happy Shopping';
        }
    }
}
