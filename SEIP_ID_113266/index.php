<?php
    include_once ('raw/init.php');

    if(isset($_SESSION['user_id']))
    {
        $user_id = $_SESSION['user_id'];
        if($user_id !== NULL)
        {
            redirect("home.php");
        }
    }
    
    
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>MSOUL | Login Or Sing Up</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Circle Hover Effects with CSS Transitions" />
        <meta name="keywords" content="circle, border-radius, hover, css3, transition" />
        <meta name="author" content="Codrops" />
        <link rel="icon" href="asset/front_end/images/icon.ico"> 
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /><!-- BOOTSTRAP (CDN) -->
        <link rel="stylesheet" type="text/css" href="asset/front_end/css/bootstrap.min.css" /><!-- BOOTSTRAP (UI) -->
        <link rel="stylesheet" type="text/css" href="asset/front_end/css/normalize.css" /><!-- NORMALIZE -->
        <link rel="stylesheet" type="text/css" href="asset/front_end/css/style.css" /><!-- CUSTOM -->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700' rel='stylesheet' type='text/css' />
        <script type="text/javascript" src="asset/front_end/js/modernizr.custom.79639.js"></script> 
        <!--[if lte IE 8]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
    </head>
    <body>
        <div class="container">
            <!----------- user login --------------->
            <?php include_once ('user_registration.php'); ?>
            <!------------user login ---------------->
            <!--===================================-->
            <!----------- user login --------------->
            <?php include_once ('user_login.php'); ?>
            <!------------user login ---------------->
        </div>
        
        <!-- JAVASCRIPT -->

        <!-- JS jQUERY 2 -->
        <script src="asset/front_end/js/jquery.min.js"></script>
        
        <!-- JS jQUERY 1.9.0-->
        <!--<script src="asset/front_end/js/jquery-1.9.0.min.js"></script>-->
        
        <!-- Bootstrap -->
        <script src="asset/front_end/js/bootstrap.js"></script>
        
        <!-- Jsval (VALIDATION) -->
        <script src="asset/front_end/js/jsval.js"></script>
        
        <!-- Ajax (EMAIL CHECKING) -->
        <script src="asset/front_end/js/ajax.js"></script>

        <!-- CUSTOM JS -->
        <script type="asset/front_end/text/javascript" src="js/script.js"></script>
        
        <script type="asset/front_end/text/javascript" src="js/script.js"></script>
    </body>
</html>
