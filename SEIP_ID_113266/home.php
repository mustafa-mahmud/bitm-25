<?php
include_once ('./raw/init.php');
$result_user = select_all_user_info();

$query_result = select_all_user_info_limit();

$query_status = select_all_users_status();

if(isset($_GET['status_id']))
{
    increase_likes($_GET['status_id']);
    redirect('home.php');
}

if(isset($_POST['btn_comment']))
{
    give_comment($_POST);
}

if(isset($_POST['status_bnt']))
{
    give_status($_POST,$_FILES);
}

$user_id = $_SESSION['user_id'];
    if($user_id == NULL)
    {
        redirect("index.php");
    }

    if(isset($_GET['status']))
    {
        user_logout();
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php include_once ('asset/include/header.php'); ?>
    </head>
    <body>
        <!-- start of nav -->
        <?php include_once ('asset/include/nav.php'); ?>
        <!-- end of nav -->
        <section class="home-container">
            <div class="row">
                <div class="col-md-2">
                    <div class="list-group">
                        <p class="list-group-item">MAN SOUL</p>
                        <h4 class="list-group-item" style="background-color: #111; text-align: center;">
                            <a href="home.php" style="text-decoration: none; color: #fff;">
                                <?php if(loged_in()){ echo $_SESSION['first_name'] .' '; echo $_SESSION['last_name']; } else { redirect("index.php"); } ?>
                            </a>
                        </h4>
                        <a href="home.php" class="list-group-item">News Grounds</a>
                    </div>
                </div><!-- end of col-md-2 -->
                <div class="col-md-7">
                    <?php include_once ('status.php'); ?>
                    <?php while ($row = fetch_assoc($query_status)) { ?>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <a href="#" class="post-avator thumbnail">
                                        <img src="<?php
                                        $users = select_user_by_id($row['user_id']);
                                        $user = fetch_assoc($users);
                                        echo $user['users_avator']; ?>" style="width: 64px; height: 64px;"/>
                                        <div class="text-center"><?php echo $user['first_name']; ?></div>
                                    </a>
                                    <div class="text-center"><i class="fa fa-thumbs-o-up"></i> <?php echo $row['status_likes']; ?></div>
                                </div><!-- end of col-sm-2 -->
                                <div class="col-sm-8">
                                    <div class="bubble">
                                        <div class="pointer">
                                            <p class="text-justify"><?php echo $row['status_description']; ?></p>
                                            <?php
                                            if($row['status_images'] != NULL)
                                            {
                                                echo "<img src=\"".  $row['status_images']. "\"" 
                                                    . "alt=\"". "NO --- Image". "\"". "class=\"img-thumbnail\" style=\"width: 450px; height:350px;\">";
                                            }
                                            ?>
                                        </div>
                                        <div class="pointer-border"></div>
                                    </div><!-- end of bubble -->
                                    <p class="post-action"><a href="profile.php?status_id=<?php echo $row['status_id']; ?>">Like</a> - <a href="#">Comment</a></p>
                                    <div class="comment-form">
                                        <form class="form-inline" action="" method="post" act="">
                                            <div class="form-group">
                                                <input type="text" name="comment_description" placeholder="Comment" class="form-control" />
                                                <input type="submit" value="Comment" name="btn_comment" class="btn btn-default form-control" />
                                                <input type="hidden" value="<?php echo $row['status_id']; ?>" name="id" />
                                            </div>
                                        </form>
                                    </div><!-- end of comment -->
                                    <div class="clearfix"></div>
                                    <div>
                                        <?php $comment_result = select_all_comment_by_comment_id($row['status_id']);
                                        while ($row = fetch_assoc($comment_result)) { ?>
                                        <div class="comment">
                                            <a href="user_id.php?member=user&user_id=<?php echo $row['user_id']; ?>" class="comment-avator pull-left">
                                                <img src="<?php
                                        $user_comments = select_user_by_id($row['user_id']);
                                        $user_comment = fetch_assoc($user_comments);
                                        echo $user_comment['users_avator']; ?>" alt="<?php echo $user_comment['first_name']; ?>" style="width: 28px; height: 28px;" class="img-rounded" />
                                            </a>
                                            <div class="comment-text">
                                                <p><?php echo $row['comment_description']; ?></p>
                                            </div>
                                        </div><!-- end of comment -->
                                        <?php } ?>
                                    </div><!-- end of comments -->
                                    <div class="clearfix"></div> 
                                </div><!-- end of col-sm-8 -->
                            </div><!-- end of row -->
                        </div><!-- end of plane-body -->
                    </div><!-- end of post -->
                    <?php } ?>
                </div><!-- end of col-md-7 -->
                <div class="col-md-3">
                    <div class="panel panel-default firends">
                        <div class="panel-heading">
                            <h3 class="panel-title"><b>Add Friends</b></h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-inline">
                                <?php while ($row = fetch_assoc($query_result)) { ?>
                                <li><a href="user_id.php?member=user&user_id=<?php echo $row['user_id']; ?>" class="thumbnail" style="text-decoration: none; text-align: center;"><img src="<?php echo $row['users_avator']; ?>" alt="user" style="width: 64px; height: 64px;"><span style="font-size: 10px;"><?php echo $row['first_name']; ?></span></a></li>
                                <?php } ?>
                            </ul>
                            <div class="clearfix"></div>
                            <a href="add_friends.php" class="btn btn-primary btn-block">View All Friends</a>
                        </div>
                    </div><!-- end of add friends-->
                </div><!-- end of col-md-3 -->
            </div><!-- end of row -->

            <form class="modal fade" id="updateCreditCard">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                            <h4 class="modal-title"><span class="fa fa-users"></span> Create New Room</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="roomName">Room Name</label>
                                <input type="text" placeholder="room name" class="form-control" id="roomName">
                            </div>
                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" name="privacy">Public Room</label>
                                <label class="radio-inline"><input type="radio" name="privacy">Closed Room</label>
                                <label class="radio-inline"><input type="radio" name="privacy">Protected Room</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <a href="room.html" class="btn btn-primary">Create Room</a>
                        </div>
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </form>
        </section><!-- end of section -->
        <?php include_once ('asset/include/footer.php'); ?>
    </body>
</html>
