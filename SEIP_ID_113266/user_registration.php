<div class="col-md-6">
    <div class="">
        <div class="img"></div>
        <div>
            <a href="index.php" title="msoul" class="">ManSoul</a>
        </div>
    </div>
    <div>
        <h3>Join with our network and enjoying and sharing feeling with secretly!</h3>
    </div>
    <form class="clearfix" action="" method="post" onsubmit="validateStandard(this)">
        <div class="form-group col-md-10">
            <input type="text" name="first_name" placeholder="First Name" class="form-control" required="required" regexp="JSVAL_RX_ALPHA" err="User valied name">
        </div>
        <div class="form-group col-md-10">
            <input type="text" name="last_name" placeholder="Last Name" class="form-control" required="required" regexp="JSVAL_RX_ALPHA" err="User valied Nname">
        </div>
        <div class="form-group col-md-10">
            <input type="email" name="email" placeholder="Email" class="form-control" required="required" regexp="JSVAL_RX_EMAIL" err="Invalied format email address">
        </div>
        <div class="form-group col-md-10">
            <input type="password" name="password" placeholder="Password" class="form-control" required="required">
        </div>
        <div class="form-group col-md-10">
            <input type="password" name="confirm_password" equals="password" placeholder="Re-Password" err="Password and Repassword mustbe same" class="form-control" required="required">
        </div>
        <div class="form-group col-md-10">
            <input type="date" name="date_of_birth" class="form-control" required="required" regexp="JSVAL_RX_DATE" err="Please input your Date of Birth">
        </div>
        <div class="col-md-10 form-inline">
            <label><input type="radio" name="gender" value="1" class="form-control radio" required="required"> Male</label>
            <label><input type="radio" name="gender" value="2" class="form-control radio" required="required"> Female</label>
        </div>
        <div class="form-group col-md-10">
            <input type="submit" name="btnReg" value="Registration" class="form-control btn btn-default btn-block">
        </div>
    </form>
</div>