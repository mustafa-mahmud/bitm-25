<?php


//* Assignment Operator(=, +=, -=, *=, /=, %=, .=);

//$x=10;
//$y=20;
//echo $x+=$y; //$x=$x+$y
//
//echo '<br/>';
//
//echo $x-=$y; //$x=$x-$y
//
//echo '<br/>';
//
//echo $x*=$y; //$x=$x*$y
//
//echo '<br/>';
//
//echo $x/=$y; //$x=$x/$y
//
//echo '<br/>';
//
//echo $x%=$y; //$x=$x%$y
//
//echo '<br/>';
//
//echo $x.=$y; //$x=$x.$y
//
//echo '<br/>';

//* Conditional Operator(>, >=, <, <=, ==, !=, ===, !==);

//$x=20;
//$y=20;
//
//echo $x>$y;
//echo '<br/>';
//echo $x>=$y;
//echo '<br/>';
//echo $x<$y;
//echo '<br/>';
//echo $x<=$y;
//echo '<br/>';
//echo $x==$y; //value equal but data type difference
//echo '<br/>';
//echo $x!=$y;
//echo '<br/>';
//echo $x===$y; //check value equal
//echo '<br/>';
//echo $x!==$y;
//echo '<br/>';


//* Logical Operator(&&, || ,!)


//$x=10;
//$y=20;
//$z=30;
//
//echo($x<$y)&&($y<$z);
//echo '<br/>';
//echo($x<$y)||($y<$z);
//echo '<br/>';

//$x=true;
//echo $x;
